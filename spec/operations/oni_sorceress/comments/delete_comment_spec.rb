# frozen_string_literal: true

RSpec.describe OniSorceress::Comments::DeleteComment do
  it "deletes comment permanently" do
    comment = create(
      :oni_sorceress_content_comment,
      request: create(:oni_sorceress_analytics_request),
      spam_checks: [create(:oni_sorceress_analytics_spam_check)],
    )
    request = comment.request
    spam_check = comment.spam_checks.first

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(OniSorceress::Content::Comment.find_by(id: comment.id)).to be(nil)
    expect(OniSorceress::Analytics::Request.find_by(id: request.id)).to be(nil)
    expect(OniSorceress::Analytics::SpamCheck.find_by(id: spam_check.id)).to be(nil)
  end

  it "returns errors if deletion fails" do
    comment = create(
      :oni_sorceress_content_comment,
      request: create(:oni_sorceress_analytics_request),
      spam_checks: [create(:oni_sorceress_analytics_spam_check)],
    )

    request = comment.request
    spam_check = comment.spam_checks.first

    allow(comment).to receive_messages(destroy: false, errors: instance_double(ActiveModel::Errors, full_messages: ["some error"]))
    allow(OniSorceress::Content::Comment).to receive(:find).and_return(comment)

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_failure
    expect(result[:errors]).to eq(["Cannot delete comment", "some error"])
    expect(OniSorceress::Content::Comment.find_by(id: comment.id)).to be_present
    expect(OniSorceress::Analytics::Request.find_by(id: request.id)).to be_present
    expect(OniSorceress::Analytics::SpamCheck.find_by(id: spam_check.id)).to be_present
  end
end
