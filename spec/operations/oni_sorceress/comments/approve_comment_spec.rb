# frozen_string_literal: true

RSpec.describe OniSorceress::Comments::ApproveComment do
  it "sets published_at" do
    comment = create(:oni_sorceress_content_comment, published_at: nil)

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(comment.reload.published_at).to be_present
  end

  it "adds manual ham spam check" do
    comment = create(:oni_sorceress_content_comment, spam_checks: [])

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(comment.reload.spam_checks.count).to eq(1)
    expect(comment.reload.spam_checks.first.attributes).to include({
      "classifier" => "manual",
      "result" => "ham",
      "score" => 100,
    })
  end

  it "returns errors if approval fails" do
    comment = create(:oni_sorceress_content_comment, published_at: nil)
    allow(comment).to receive_messages(update: false, errors: instance_double(ActiveModel::Errors, full_messages: ["some error"]))
    allow(OniSorceress::Content::Comment).to receive(:find).and_return(comment)

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_failure
    expect(result[:errors]).to eq(["Comment: some error"])
    expect(comment.reload.published_at).to be(nil)
    expect(comment.reload.spam_checks.count).to eq(0)
  end

  it "returns errors if adding ham check fails" do
    comment = create(:oni_sorceress_content_comment, published_at: nil)
    allow(OniSorceress::Comments::MarkCommentAsHam).to receive(:call).and_return(instance_double(
      Trailblazer::Operation::Railway::Result,
      failure?: true,
      :[] => ["some error"],
    ))

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_failure
    expect(result[:errors]).to eq(["some error"])
    expect(comment.reload.published_at).to be(nil)
    expect(comment.reload.spam_checks.count).to eq(0)
  end
end
