# frozen_string_literal: true

RSpec.describe OniSorceress::Comments::MarkCommentAsHam do
  let(:bogofilter) { Bogofilter.new(dbpath: Rails.application.config.bogofilter_database) }

  it "marks as HAM, learns Bogofilter" do
    allow(Bogofilter).to receive(:new).and_return(bogofilter)
    allow(bogofilter).to receive(:add_ham).and_call_original
    comment = create(:oni_sorceress_content_comment)

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(comment.reload.bogofilter_hams.count).to eq(1)
    expect(comment.reload.published_at).to be(nil)
    expect(comment.spam_checks.first.attributes).to include({
      "classifier" => "manual",
      "result" => "ham",
      "score" => 100,
    })
    expect(bogofilter).to have_received(:add_ham)
  end

  it "marks as HAM, skips learning Bogofilter if already learned" do
    allow(Bogofilter).to receive(:new).and_return(bogofilter)
    allow(bogofilter).to receive(:add_ham)
    allow(bogofilter).to receive(:add_spam)
    comment = create(
      :oni_sorceress_content_comment,
      bogofilter_hams: [
        build(:oni_sorceress_analytics_bogofilter_ham, learn_state: :learned),
      ],
    )

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(bogofilter).not_to have_received(:add_ham)
    expect(bogofilter).not_to have_received(:add_spam)
  end

  it "marks as HAM, unlearns Bogofilter SPAM if learned as spam before" do
    allow(Bogofilter).to receive(:new).and_return(bogofilter)
    allow(bogofilter).to receive(:remove_spam).and_return(true)

    comment = create(
      :oni_sorceress_content_comment,
      bogofilter_spams: [
        build(:oni_sorceress_analytics_bogofilter_spam, learn_state: :learned),
        build(:oni_sorceress_analytics_bogofilter_spam, learn_state: :learned),
        build(:oni_sorceress_analytics_bogofilter_spam, learn_state: :not_learned),
      ],
    )

    result = described_class.call(comment_id: comment.id)

    expect(result).to be_success
    expect(bogofilter).to have_received(:remove_spam).twice
  end
end
