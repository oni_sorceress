# frozen_string_literal: true

require "rails_helper"

[nil, "www."].each do |subdomain|
  RSpec.describe "show author for #{subdomain.inspect} domain" do
    it "shows author without any articles from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      author = create(:oni_sorceress_content_author, name: "Sheppard", world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:ok)
      expect(response.body).to include("Sheppard")
    end

    it "does not show author without any articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      author = create(:oni_sorceress_content_author, name: "Wrex", world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "does not show author with only hidden articles from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      author = create(:oni_sorceress_content_author, name: "Tali", world:)
      create(:oni_sorceress_content_article, hidden: true, author:, world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "does not show author with only hidden articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      author = create(:oni_sorceress_content_author, name: "Liara", world:)
      create(:oni_sorceress_content_article, hidden: true, author:, world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "shows author with non-hidden and hidden articles from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      author = create(:oni_sorceress_content_author, name: "Shadow Broker", world:)
      create(:oni_sorceress_content_article, hidden: true, author:, world:)
      create(:oni_sorceress_content_article, hidden: false, author:, world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:ok)
      expect(response.body).to include("Shadow Broker")
    end

    it "does not show author with non-hidden and hidden articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      author = create(:oni_sorceress_content_author, name: "Garrus", world:)
      create(:oni_sorceress_content_article, hidden: true, author:, world:)
      create(:oni_sorceress_content_article, hidden: false, author:, world:)

      get author_path(author.name), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end
  end
end
