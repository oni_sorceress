# frozen_string_literal: true

require "rails_helper"

RSpec.describe "show article to bots, search bots or preview bots for naked domain" do
  it "returns not found when article doesn't exist" do
    get article_path("foobar"), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "oni sorceress bot" }

    expect(response).to have_http_status(:not_found)
    expect(response.body).to include("(page not found)")
  end

  it "returns not found when non-hidden article exists in different world" do
    world = create(:oni_sorceress_content_world, name: "foo")
    article = create(:oni_sorceress_content_article, hidden: false, body: "Far Rim", world:)

    get article_path(article.pretty_id), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "oni sorceress bot" }

    expect(response).to have_http_status(:not_found)
    expect(response.body).to include("(page not found)")
  end

  it "returns not found when hidden article exists in different world" do
    world = create(:oni_sorceress_content_world, name: "foo")
    article = create(:oni_sorceress_content_article, hidden: true, body: "Apien Crest", world:)

    get article_path(article.pretty_id), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "oni sorceress bot" }

    expect(response).to have_http_status(:not_found)
    expect(response.body).to include("(page not found)")
  end

  it "returns ok for non-hidden article" do
    world = create(:oni_sorceress_content_world, name: "root")
    article = create(:oni_sorceress_content_article, hidden: false, body: "Andromeda", world:)

    get article_path(article.pretty_id), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "oni sorceress bot" }

    expect(response).to have_http_status(:ok)
    expect(response.body).to include("Andromeda")
  end

  it "returns not found to bots for hidden articles" do
    world = create(:oni_sorceress_content_world, name: "root")
    article = create(:oni_sorceress_content_article, hidden: true, body: "Horsehead", world:)

    get article_path(article.pretty_id), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "oni sorceress bot" }

    expect(response).to have_http_status(:not_found)
    # use the same not found text as for other resources, so it's not leaking any info
    expect(response.body).to include("(page not found)")
  end

  it "returns not found to external preview bots for hidden articles" do
    world = create(:oni_sorceress_content_world, name: "root")
    article = create(:oni_sorceress_content_article, hidden: true, body: "Hades Gamma", world:)

    get article_path(article.pretty_id), headers: { HTTP_HOST: "example.com", HTTP_USER_AGENT: "SkypeUriPreview" }
    expect(response).to have_http_status(:not_found)
    # use the same not found text as for other resources, so it's not leaking any info
    expect(response.body).to include("(page not found)")
  end
end
