# frozen_string_literal: true

require "rails_helper"

RSpec.describe "article comment" do
  it "don't allow to create more than 100k comments" do
    allow(OniSorceress::Content::Comment).to receive(:count).and_return(100_000)
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "Pathfinder",
        body: "New mission",
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:alert]).to include("Can't create more comments. Don't fuck with the system!")
  end

  it "don't allow to post an empty comment" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "Pathfinder", body: " ",
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:alert]).to include("Can't create comment. Maybe empty?")
    expect(article.reload.comments.count).to eq(0)
  end

  it "allows empty poster name" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "",
        body: "New mission",
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:notice]).to include("Comment added, but needs to be approved by admin")
  end

  it "allows 4k comment body" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "",
        body: "1" * 4000,
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:notice]).to include("Comment added, but needs to be approved by admin")
  end

  it "allows 100 chars of poster name" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "1" * 100,
        body: "New mission",
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:notice]).to include("Comment added, but needs to be approved by admin")
  end

  it "don't allow more than 4k comment body" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "Pathfinder",
        body: "1" * 4001,
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:alert]).to include("Can't create comment. Maybe empty?")
  end

  it "don't allow more than 100 chars of poster name" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "1" * 101,
        body: "New Mission",
      },
    }

    expect(response).to redirect_to(article_path(article.pretty_id))
    expect(flash[:alert]).to include("Can't create comment. Maybe empty?")
  end

  it "adds bogofilter spam check to comment" do
    article = create(:oni_sorceress_content_article)

    post article_comments_path(article.id), params: {
      oni_sorceress_content_comment: {
        poster_name: "Ryder",
        body: "New mission",
      },
    }

    comment = OniSorceress::Content::Comment.last
    expect(comment.spam_checks.count).to eq(1)

    spam_check = comment.spam_checks.first
    expect(spam_check.classifier).to eq("bogofilter")
    expect(spam_check.result).to eq("unsure")
    expect(spam_check.score).to eq(0.52)
  end
end
