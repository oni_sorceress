# frozen_string_literal: true

require "rails_helper"

# rubocop:disable RSpec/ExampleLength
[nil, "www."].each do |subdomain|
  RSpec.describe "show second level category for #{subdomain.inspect} domain" do
    it "does not show second level category without any articles from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      create(
        :oni_sorceress_content_category,
        first_level: "Nimbus Cluster",
        second_level: "Agaiou",
        world:,
      )

      get second_level_category_path(first_level: "Nimbus Cluster", second_level: "Agaiou"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "does not show second level category without any articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      create(
        :oni_sorceress_content_category,
        first_level: "Ninmah Cluster",
        second_level: "Mulla Xul",
        world:,
      )

      get second_level_category_path(first_level: "Ninmah Cluster", second_level: "Mulla Xul"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "does not show second level category with only hidden articles from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      category = create(
        :oni_sorceress_content_category,
        first_level: "Nubian Expanse",
        second_level: "Dakka",
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: true,
        last_updated_at: 1.minute.ago,
        category:,
        world:,
      )

      get second_level_category_path(first_level: "Nubian Expanse", second_level: "Dakka"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "does not show second level category with only hidden articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      category = create(
        :oni_sorceress_content_category,
        first_level: "Perseus Veil",
        second_level: "Tikkun",
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: true,
        last_updated_at: 1.minute.ago,
        category:,
        world:,
      )

      get second_level_category_path(first_level: "Perseus Veil", second_level: "Tikkun"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    it "shows second level category with non-hidden and hidden articles from root world with non-hidden only" do
      world = create(:oni_sorceress_content_world, name: "root")
      category1 = create(
        :oni_sorceress_content_category,
        first_level: "Serpent Nebula",
        second_level: "Boltzmann",
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: true,
        last_updated_at: 1.minute.ago,
        title: "Tyre",
        category: category1,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: false,
        last_updated_at: 1.minute.ago,
        title: "Elysium",
        category: category1,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: false,
        last_updated_at: 1.minute.from_now,
        title: "Grissom Academy",
        category: category1,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: false,
        last_updated_at: nil,
        title: "Sidon",
        category: category1,
        world:,
      )

      different_category = create(
        :oni_sorceress_content_category,
        first_level: "Ismar Frontier",
        second_level: "Aquila",
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: true,
        last_updated_at: 1.minute.ago,
        title: "Lepini",
        category: different_category,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: false,
        last_updated_at: 1.minute.ago,
        title: "Vecchio",
        category: different_category,
        world:,
      )

      get second_level_category_path(first_level: "Serpent Nebula", second_level: "Boltzmann"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:ok)
      expect(response.body).to include("Serpent Nebula")

      expect(response.body).to include("Boltzmann")
      expect(response.body).not_to include("Tyre")
      expect(response.body).to include("Elysium")
      expect(response.body).not_to include("Grissom Academy")
      expect(response.body).not_to include("Sidon")

      expect(response.body).not_to include("Lepini")
      expect(response.body).not_to include("Vecchio")
    end

    it "does not show second level category with non-hidden and hidden articles from different world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      category = create(
        :oni_sorceress_content_category,
        first_level: "Pylos Nebula",
        second_level: "Dirada",
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: true,
        last_updated_at: 1.minute.ago,
        category:,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        hidden: false,
        last_updated_at: 1.minute.ago,
        category:,
        world:,
      )

      get second_level_category_path(first_level: "Pylos Nebula", second_level: "Dirada"), headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end
  end
end
# rubocop:enable RSpec/ExampleLength
