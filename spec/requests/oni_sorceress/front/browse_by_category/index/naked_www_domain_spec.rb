# frozen_string_literal: true

require "rails_helper"

[nil, "www."].each do |subdomain|
  RSpec.describe "browse by categories for #{subdomain.inspect} domain" do
    describe "articles" do
      it "returns not found if there are no articles" do
        get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

        expect(response).to have_http_status(:not_found)
        expect(response.body).to include("(page not found)")
      end

      context "when posted in the future" do
        it "does not include hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "durak",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("durak")
        end

        it "does not include hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "kanin",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kanin")
        end

        it "does not include non-hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kruban",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kruban")
        end

        it "does not include non-hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "tuchanka",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("tuchanka")
        end
      end

      context "when posted in the past" do
        it "does not include hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "ruam",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("ruam")
        end

        it "does not include hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "vaul",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("vaul")
        end

        it "includes non-hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kelim",
            last_updated_at: 1.minute.ago,
            world:,
          )
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "thesalgon",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).to include("/articles/kelim")
          expect(response.body).to include("/articles/thesalgon")
        end

        it "does not include non-hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "dor",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get browse_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("dor")
        end
      end
    end
  end
end
