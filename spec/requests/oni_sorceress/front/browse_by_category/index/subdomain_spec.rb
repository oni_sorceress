# frozen_string_literal: true

require "rails_helper"

RSpec.describe "browse by categories for subdomain" do
  describe "articles" do
    it "returns not found if there are no articles" do
      get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("(page not found)")
    end

    context "when posted in the future" do
      it "does not include hidden articles posted from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "cronos-station",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("cronos-station")
      end

      it "does not include hidden articles posted from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "maganlis",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("maganlis")
      end

      it "does not include hidden articles posted from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "therumlon",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("therumlon")
      end

      it "does not include non-hidden articles posted from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "amaranthine",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("amaranthine")
      end

      it "does not include non-hidden articles posted from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "wentania",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("wentania")
      end

      it "does not include non-hidden articles posted from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "svarog",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("svarog")
      end
    end

    context "when posted in the past" do
      it "does not include hidden articles from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "noveria",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("noveria")
      end

      it "does not include hidden articles from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "morana",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("morana")
      end

      it "does not include hidden articles from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "veles",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("veles")
      end

      it "does not include non-hidden articles from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "yunthorl",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("yunthorl")
      end

      it "does not include non-hidden articles from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "antitarra",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("antitarra")
      end

      it "includes non-hidden articles from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "trelyn",
          last_updated_at: 1.minute.ago,
          world:,
        )
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "xawin",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get browse_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).to include("/articles/trelyn")
        expect(response.body).to include("/articles/xawin")
      end
    end
  end
end
