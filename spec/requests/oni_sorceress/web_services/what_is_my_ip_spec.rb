# frozen_string_literal: true

require "rails_helper"

RSpec.describe "what is my ip" do
  it "shows current IP of client" do
    get what_is_my_ip_address_path

    expect(response).to have_http_status(:ok)
    expect(response.body).to include("127.0.0.1")
  end
end
