# frozen_string_literal: true

require "rails_helper"

[nil, "www."].each do |subdomain|
  RSpec.describe "sitemap.xml for #{subdomain.inspect} domain" do
    it "is valid sitemap file" do
      world = create(:oni_sorceress_content_world, name: "root")
      create(
        :oni_sorceress_content_category,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        last_updated_at: 1.minute.ago,
        world:,
      )

      get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

      doc = Nokogiri.XML(response.body, &:strict)
      xsd = Nokogiri::XML::Schema(Rails.root.join("spec/support/sitemap.xsd").read)

      expect(xsd.validate(doc).map(&:message)).to be_empty
    end

    it "includes root URL" do
      get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

      expect(response.body).to include("<loc>http://#{subdomain}example.com/</loc>")
    end

    describe "categories" do
      it "includes first and second level categories from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        cat1 = create(
          :oni_sorceress_content_category,
          first_level: "Milky Way",
          second_level: "Local Cluster",
          world:,
        )
        cat2 = create(
          :oni_sorceress_content_category,
          first_level: "Shadow Sea",
          second_level: "Iera",
          world:,
        )
        create(:oni_sorceress_content_article, category: cat1, world: world)
        create(:oni_sorceress_content_article, category: cat2, world: world)

        get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

        expect(response.body).to include("<loc>http://#{subdomain}example.com/categories/Milky%20Way</loc>")
        expect(response.body).to include("<loc>http://#{subdomain}example.com/categories/Milky%20Way/Local%20Cluster</loc>")
        expect(response.body).to include("<loc>http://#{subdomain}example.com/categories/Shadow%20Sea</loc>")
        expect(response.body).to include("<loc>http://#{subdomain}example.com/categories/Shadow%20Sea/Iera</loc>")
      end

      it "does not include first and second level category from different world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        cat1 = create(
          :oni_sorceress_content_category,
          first_level: "Vallhallan Threshold",
          second_level: "Micah",
          world:,
        )
        create(:oni_sorceress_content_article, category: cat1, world: world)

        get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

        expect(response.body).not_to include("Vallhallan")
        expect(response.body).not_to include("Micah")
      end
    end

    describe "articles" do
      context "when posted in the future" do
        it "does not include hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "durak",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("durak")
        end

        it "does not include hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "kanin",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kanin")
        end

        it "does not include non-hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kruban",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kruban")
        end

        it "does not include non-hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "tuchanka",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("tuchanka")
        end
      end

      context "when posted in the past" do
        it "does not include hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "ruam",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("ruam")
        end

        it "does not include hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "vaul",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("vaul")
        end

        it "includes non-hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kelim",
            last_updated_at: 1.minute.ago,
            world:,
          )
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "thesalgon",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).to include("<loc>http://#{subdomain}example.com/articles/kelim</loc>")
          expect(response.body).to include("<loc>http://#{subdomain}example.com/articles/thesalgon</loc>")
        end

        it "does not include non-hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "dor",
            last_updated_at: 1.minute.ago,
            world:,
          )

          get sitemap_xml_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("dor")
        end
      end
    end
  end
end
