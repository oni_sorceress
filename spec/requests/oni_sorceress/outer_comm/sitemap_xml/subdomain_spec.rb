# frozen_string_literal: true

require "rails_helper"

RSpec.describe "sitemap.xml for subdomain" do
  it "is valid sitemap file" do
    world = create(:oni_sorceress_content_world, name: "darkspace")
    cat1 = create(
      :oni_sorceress_content_category,
      world:,
    )
    create(
      :oni_sorceress_content_article,
      category: cat1,
      last_updated_at: 1.minute.ago,
      world:,
    )

    get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

    doc = Nokogiri.XML(response.body, &:strict)
    xsd = Nokogiri::XML::Schema(Rails.root.join("spec/support/sitemap.xsd").read)

    expect(xsd.validate(doc).map(&:message)).to be_empty
  end

  it "includes root URL" do
    get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

    expect(response.body).to include("<loc>http://darkspace.example.com/</loc>")
  end

  describe "categories" do
    it "does not include first and second level categories from root world" do
      world = create(:oni_sorceress_content_world, name: "root")
      cat1 = create(
        :oni_sorceress_content_category,
        first_level: "Milky Way",
        second_level: "Local Cluster",
        world:,
      )
      create(:oni_sorceress_content_article, category: cat1, world: world)

      get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

      expect(response.body).not_to include("Milky")
      expect(response.body).not_to include("Local")
    end

    it "does not include first and second level category from different world" do
      world = create(:oni_sorceress_content_world, name: "meow")
      cat1 = create(
        :oni_sorceress_content_category,
        first_level: "Vallhallan Threshold",
        second_level: "Micah",
        world:,
      )
      create(:oni_sorceress_content_article, category: cat1, world: world)

      get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

      expect(response.body).not_to include("Vallhallan")
      expect(response.body).not_to include("Micah")
    end

    it "includes first and second level categories from selected world" do
      world = create(:oni_sorceress_content_world, name: "darkspace")
      cat1 = create(
        :oni_sorceress_content_category,
        first_level: "Rosetta Nebula",
        second_level: "Enoch",
        world:,
      )
      cat2 = create(
        :oni_sorceress_content_category,
        first_level: "Far Rim",
        second_level: "Ma-at",
        world:,
      )
      create(:oni_sorceress_content_article, category: cat1, world: world)
      create(:oni_sorceress_content_article, category: cat2, world: world)

      get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

      expect(response.body).to include("<loc>http://darkspace.example.com/categories/Rosetta%20Nebula</loc>")
      expect(response.body).to include("<loc>http://darkspace.example.com/categories/Rosetta%20Nebula/Enoch</loc>")
      expect(response.body).to include("<loc>http://darkspace.example.com/categories/Far%20Rim</loc>")
      expect(response.body).to include("<loc>http://darkspace.example.com/categories/Far%20Rim/Ma-at</loc>")
    end
  end

  describe "articles" do
    context "when posted in the future" do
      it "does not include hidden articles posted from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "cronos-station",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("cronos-station")
      end

      it "does not include hidden articles posted from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "maganlis",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("maganlis")
      end

      it "does not include hidden articles posted from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "therumlon",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("therumlon")
      end

      it "does not include non-hidden articles posted from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "amaranthine",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("amaranthine")
      end

      it "does not include non-hidden articles posted from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "wentania",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("wentania")
      end

      it "does not include non-hidden articles posted from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "svarog",
          last_updated_at: 1.minute.from_now,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("svarog")
      end
    end

    context "when posted in the past" do
      it "does not include hidden articles from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "noveria",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("noveria")
      end

      it "does not include hidden articles from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "morana",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("morana")
      end

      it "does not include hidden articles from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: true,
          pretty_id: "veles",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("veles")
      end

      it "does not include non-hidden articles from root world" do
        world = create(:oni_sorceress_content_world, name: "root")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "yunthorl",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("yunthorl")
      end

      it "does not include non-hidden articles from different world" do
        world = create(:oni_sorceress_content_world, name: "meow")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "antitarra",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).not_to include("antitarra")
      end

      it "includes non-hidden articles from selected world" do
        world = create(:oni_sorceress_content_world, name: "darkspace")
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "trelyn",
          last_updated_at: 1.minute.ago,
          world:,
        )
        create(
          :oni_sorceress_content_article,
          hidden: false,
          pretty_id: "xawin",
          last_updated_at: 1.minute.ago,
          world:,
        )

        get sitemap_xml_path, headers: { HTTP_HOST: "darkspace.example.com" }

        expect(response.body).to include("<loc>http://darkspace.example.com/articles/trelyn</loc>")
        expect(response.body).to include("<loc>http://darkspace.example.com/articles/xawin</loc>")
      end
    end
  end
end
