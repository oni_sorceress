# frozen_string_literal: true

require "rails_helper"

[nil, "www."].each do |subdomain|
  RSpec.describe "allfeed.atom for #{subdomain.inspect} domain" do
    before do
      @old_title = ENV.fetch("META_TITLE_SUFFIX", nil)
      @old_desc = ENV.fetch("META_DESCRIPTION", nil)

      ENV["META_TITLE_SUFFIX"] = "AAAAAA TITLE"
      ENV["META_DESCRIPTION"] = "BBBBBBB DESCRIPTION"
    end

    after do
      ENV["META_TITLE_SUFFIX"] = @old_title
      ENV["META_DESCRIPTION"] = @old_desc
    end

    it "is valid Atom file" do
      world = create(:oni_sorceress_content_world, name: "root")
      create(
        :oni_sorceress_content_category,
        world:,
      )
      create(
        :oni_sorceress_content_article,
        last_updated_at: 1.minute.ago,
        world:,
      )

      get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

      doc = Nokogiri.XML(response.body, &:strict)
      rng = Nokogiri::XML::RelaxNG(Rails.root.join("spec/support/atom.rng").read)

      expect(rng.validate(doc).map(&:message)).to be_empty
    end

    it "has feed details" do
      world = create(:oni_sorceress_content_world, name: "root")
      article = create(
        :oni_sorceress_content_article,
        pretty_id: "silean-nebula",
        last_updated_at: 1.minute.ago,
        world:,
      )

      get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

      doc = Nokogiri.XML(response.body, &:strict)
      feed = doc.css("feed").first
      expect(feed.css("> link").to_s).to eq("<link rel=\"self\" type=\"application/atom+xml\" \
href=\"http://#{subdomain}example.com/allfeed.atom\"/>")
      expect(feed.css("> title").text).to eq("AAAAAA TITLE All")
      expect(feed.css("> subtitle").text).to eq("BBBBBBB DESCRIPTION")
      expect(feed.css("> updated").text).to eq(article.last_updated_at.iso8601)
      expect(feed.css("> generator").text).to eq("oni sorceress")
      expect(feed.css("> generator").attr("uri").value).to eq("https://repo.or.cz/oni_sorceress.git")
      expect(feed.css("> rights").text).to eq("See respective licenses in the content")
    end

    it "is last updated in 1970 when no content" do
      get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

      doc = Nokogiri.XML(response.body, &:strict)
      feed = doc.css("feed").first
      expect(feed.css("> updated").text).to eq(Time.zone.local(1970, 1, 1).iso8601)
    end

    describe "articles" do
      context "when posted or updated in the future" do
        it "does not include hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "durak",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("durak")
        end

        it "does not include hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "kanin",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kanin")
        end

        it "does not include non-hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kruban",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("kruban")
        end

        it "does not include non-hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "tuchanka",
            last_updated_at: 1.minute.from_now,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("tuchanka")
        end
      end

      context "when posted or updated more than 3 months ago" do
        it "does not include hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "agnin",
            last_updated_at: 3.months.ago - 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("agnin")
        end

        it "does not include hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "shasu",
            last_updated_at: 3.months.ago - 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("shasu")
        end

        it "does not include non-hidden articles posted from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "dranen",
            last_updated_at: 3.months.ago - 10.minutes,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("dranen")
        end

        it "does not include non-hidden articles posted from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "mahavid",
            last_updated_at: 3.months.ago - 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("mahavid")
        end
      end

      context "when posted or updated less than 3 months ago" do
        it "does not include hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "ruam",
            last_updated_at: 3.months.ago + 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("ruam")
        end

        it "does not include hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: true,
            pretty_id: "vaul",
            last_updated_at: 3.months.ago + 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("vaul")
        end

        it "includes non-hidden articles from root world" do
          world = create(:oni_sorceress_content_world, name: "root")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "kelim",
            last_updated_at: 3.months.ago + 1.minute,
            title: "Kelim",
            world:,
          )
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "thesalgon",
            last_updated_at: 3.months.ago + 1.minute,
            title: "Thesalgon",
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          doc = Nokogiri.XML(response.body, &:strict)
          expect(doc.css("feed > entry").size).to eq(2)
          expect(doc.css("feed > entry > title").text).to include("Kelim")
          expect(doc.css("feed > entry > title").text).to include("Thesalgon")
        end

        it "does not include non-hidden articles from different world" do
          world = create(:oni_sorceress_content_world, name: "darkspace")
          create(
            :oni_sorceress_content_article,
            hidden: false,
            pretty_id: "dor",
            last_updated_at: 3.months.ago + 1.minute,
            world:,
          )

          get allfeed_atom_path, headers: { HTTP_HOST: "#{subdomain}example.com" }

          expect(response.body).not_to include("dor")
        end
      end
    end
  end
end
