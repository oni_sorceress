# frozen_string_literal: true

require "rails_helper"

RSpec.describe "robots.txt for subdomain" do
  it "allows all bots and all URLs" do
    get robots_txt_path, headers: { HTTP_HOST: "darkspace.example.com" }

    expect(response.body).to include("User-agent: *\nAllow: /\n")
  end

  it "does not disallow hidden articles" do
    get robots_txt_path, headers: { HTTP_HOST: "darkspace.example.com" }

    expect(response.body).not_to include("Disallow:")
  end

  it "has sitemap statement" do
    get robots_txt_path, headers: { HTTP_HOST: "darkspace.example.com" }

    expect(response.body).to include("Sitemap: http://darkspace.example.com/sitemap.xml")
  end
end
