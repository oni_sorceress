# frozen_string_literal: true

require "rails_helper"

RSpec.describe "metadata attributes" do
  describe "default langauge" do
    it "sets to en by default" do
      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(find("html")["lang"]).to eq("en")
    end

    it "takes from META_DEFAULT_LANGUAGE env variable" do
      allow(ENV).to receive(:fetch).and_call_original
      allow(ENV).to receive(:fetch).with("META_DEFAULT_LANGUAGE").and_return("sk")

      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(find("html")["lang"]).to eq("sk")
    end
  end

  describe "metadata description" do
    it "does not render if not set" do
      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(page).to have_no_selector("meta[name=description]", visible: :hidden)
    end

    it "takes description from META_DESCRIPTION env variable" do
      allow(ENV).to receive(:fetch).and_call_original
      allow(ENV).to receive(:fetch).with("META_DESCRIPTION").and_return("the best web site about Diablo 2")

      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(find("meta[name=description]", visible: :hidden)["content"]).to eq("the best web site about Diablo 2")
    end
  end

  describe "metadata title suffix" do
    it 'sets to ";; Oni Sorceress ;;" by default' do
      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(page.title).to include(";; Oni Sorceress ;;")
    end

    it "takes from META_TITLE_SUFFIX env variable" do
      allow(ENV).to receive(:fetch).and_call_original
      allow(ENV).to receive(:fetch).with("META_TITLE_SUFFIX").and_return("@ foobar")

      world = create(:oni_sorceress_content_world, name: "root")
      create(:oni_sorceress_content_article, world:)

      visit root_path

      expect(page.title).to include("@ foobar")
    end
  end
end
