# frozen_string_literal: true

require "rails_helper"

RSpec.describe "article comments functionality" do
  it "lists comments, is able to mark them as spam" do
    pending "hopeless"
    visit administration_moderation_comments_path

    expect(page).to have_css("h1", text: "Comments Moderation")
  end
end
