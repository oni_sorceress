# frozen_string_literal: true

require "rails_helper"

RSpec.describe "article comments functionality" do
  it "creates a comment" do
    world = create(:oni_sorceress_content_world, name: "root")
    article = create(:oni_sorceress_content_article, world:)

    visit article_path(article.pretty_id)

    fill_in "Your Name (public)", with: "John Sheppard"
    fill_in "Your Message", with: "About my ship"
    click_on "Post Comment"

    expect(page.body).to have_text("Comment added, but needs to be approved by admin")

    comment = article.reload.comments.first
    expect(comment.poster_name).to eq("John Sheppard")
    expect(comment.body).to eq("About my ship")
    expect(comment.published_at).to be(nil)

    request = comment.request
    expect(request).not_to be(nil)
    expect(request.data["uid"]).not_to be(nil)
    expect(request.data["ORIGINAL_FULLPATH"]).to eq("/articles/#{article.id}/comments")
  end

  it "shows only published comments, sorts from oldest to newest" do
    world = create(:oni_sorceress_content_world, name: "root")
    comment = build(
      :oni_sorceress_content_comment,
      poster_name: "Johnny Silverhand",
      body: "Wake up samurai....we have city to burn!",
      published_at: Time.utc(2077, 4, 18),
      created_at: Time.utc(2077, 4, 16, 12),
    )
    next_comment = build(
      :oni_sorceress_content_comment,
      poster_name: "Butch Cassidy",
      body: "I don't want to sound like a sore loser, but when it's over, if I'm dead, kill him.",
      published_at: Time.utc(2077, 12, 11),
      created_at: Time.utc(2077, 12, 10, 13),
    )
    not_published_comment = build(
      :oni_sorceress_content_comment,
      poster_name: "Richard B. Riddick. Escaped convict. Murderer.",
      body: "It ain't me you gotta worry about",
      published_at: nil,
    )
    article = create(:oni_sorceress_content_article, comments: [comment, next_comment, not_published_comment], world:)

    visit article_path(article.pretty_id)

    expect(all(".oni-article-comment").count).to eq(2)
    expect(page).to have_css(".oni-article-comments__count", text: "Comments (2)")

    expect(page).to have_css(".oni-article-comment__poster-name", exact_text: "Johnny Silverhand")
    expect(page).to have_css(".oni-article-comment__posted-at", exact_text: "Posted: 2077-12-10 13:00:00 UTC / Approved: 2077-12-11 00:00:00 UTC")
    expect(page).to have_css(".oni-article-comment__body", exact_text: "Wake up samurai....we have city to burn!")

    expect(page).to have_css(".oni-article-comment__poster-name", exact_text: "Butch Cassidy")
    expect(page).to have_css(".oni-article-comment__posted-at", exact_text: "Posted: 2077-04-16 12:00:00 UTC / Approved: 2077-04-18 00:00:00 UTC")
    expect(page).to have_css(".oni-article-comment__body", exact_text: "I don't want to sound like a sore loser, but when it's over, if I'm dead, kill him.")

    expect(all(".oni-article-comment__posted-at").map(&:text)).to match([include("2077-12-10 13:00:00 UTC"), include("2077-04-16 12:00:00 UTC")])
  end
end
