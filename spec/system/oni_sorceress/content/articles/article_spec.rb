# frozen_string_literal: true

require "rails_helper"

RSpec.describe "article" do
  describe "show article" do
    it "disables indexing and other bot functions in meta tags for hidden article" do
      world = create(:oni_sorceress_content_world, name: "root")
      article = create(:oni_sorceress_content_article, hidden: true, world:)

      visit article_path(article.pretty_id)

      expect(find("meta[name=robots]", visible: :all)["content"]).to eq("noindex, nofollow, noarchive, nosnippet, notranslate, noimageindex")
    end

    it "sets HTML language to article language" do
      world = create(:oni_sorceress_content_world, name: "root")
      language = create(:oni_sorceress_content_language, code: "sk")
      language2 = create(:oni_sorceress_content_language, code: "en")
      article = create(
        :oni_sorceress_content_article,
        articles_languages: [
          create(:oni_sorceress_content_articles_language, language:, priority: 2),
          create(:oni_sorceress_content_articles_language, language: language2, priority: 1),
        ],
        world:,
      )

      visit article_path(article.pretty_id)

      expect(find("html", visible: :all)["lang"]).to eq("en,sk")
    end

    it "sets title, does not use entities, but UTF-8" do
      world = create(:oni_sorceress_content_world, name: "root")
      article = create(:oni_sorceress_content_article, title: "AAAA'BBBB", world:)

      visit article_path(article.pretty_id)
      expect(page.title).to eq("AAAA'BBBB @ ;; Oni Sorceress ;;")
    end
  end
end
