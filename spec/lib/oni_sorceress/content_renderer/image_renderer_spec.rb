# frozen_string_literal: true

describe OniSorceress::ContentRenderer::ImageRenderer do
  describe ".convert" do
    context "when image not uploaded to article" do
      it "does not replace image tag with html code" do
        text = "foo {{image:aaa.jpg}} bar"

        expect(described_class.convert(text:, images: [])).to eq(text)
      end
    end

    context "when there is corresponding uploaded image in article" do
      it "replaces image tag with html code" do
        image = create(:oni_sorceress_content_image)
        text = "foo {{image:cherry_blossoms.jpg}} bar"

        result = described_class.convert(text:, images: [image])
        expect(result.gsub(%r{\/rails.*?\.jpg}, "<url>")).to match_snapshot("image_component")
      end
    end
  end
end
