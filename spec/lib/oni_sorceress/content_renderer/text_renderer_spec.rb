# frozen_string_literal: true

describe OniSorceress::ContentRenderer::TextRenderer do
  describe ".convert" do
    describe "hypertext" do
      it "renders .htm extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.htm",
          text: "<p>a *very* big cat</p>",
        )).to eq("<p>a *very* big cat</p>")
      end

      it "renders .html extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.html",
          text: "<p>a *very* big cat</p>",
        )).to eq("<p>a *very* big cat</p>")
      end
    end

    describe "markdown" do
      it "renders .md extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.md",
          text: "a *very* big cat",
        )).to eq("<p>a <em>very</em> big cat</p>\n")
      end

      it "renders .mkd extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.mkd",
          text: "a *very* big cat",
        )).to eq("<p>a <em>very</em> big cat</p>\n")
      end

      it "renders .markdown extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.markdown",
          text: "a *very* big cat",
        )).to eq("<p>a <em>very</em> big cat</p>\n")
      end
    end

    describe "plaintext" do
      it "renders .txt extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.txt",
          text: "a *very* big cat\n",
        )).to eq("a *very* big cat<br>")
      end

      it "renders .text extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.text",
          text: "a *very* big cat\n",
        )).to eq("a *very* big cat<br>")
      end
    end

    describe "textile" do
      it "renders .textile extension" do
        expect(described_class.convert(
          filename: "/home/wut/foo/cat.textile",
          text: "a *very* big cat",
        )).to eq("<p>a <strong>very</strong> big cat</p>")
      end
    end
  end

  describe ".html" do
    it "returns same string" do
      expect(described_class.html(text: "<b>foo</b>")).to eq("<b>foo</b>")
    end
  end

  describe ".markdown" do
    it "converts markdown text into HTML" do
      expect(described_class.markdown(text: "a *very* big cat"))
        .to eq("<p>a <em>very</em> big cat</p>\n")
    end
  end

  describe ".plain" do
    it "converts HTML reserved characters into entities" do
      expect(described_class.plain(text: "foo<b>&quux")).to eq("foo&lt;b&gt;&amp;quux")
    end

    it "converts new lines into <br> tags" do
      expect(described_class.plain(text: "a\nb\n")).to eq("a<br>b<br>")
    end
  end

  describe ".textile" do
    it "converts textile text into HTML" do
      expect(described_class.textile(text: "a *very* big cat"))
        .to eq("<p>a <strong>very</strong> big cat</p>")
    end
  end
end
