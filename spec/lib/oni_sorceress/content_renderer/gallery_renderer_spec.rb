# frozen_string_literal: true

describe OniSorceress::ContentRenderer::GalleryRenderer do
  describe ".convert" do
    context "when gallery not uploaded to article" do
      it "does not replace gallery tag with html code" do
        text = "foo {{gallery:quux}} bar"

        expect(described_class.convert(text:, galleries: [])).to eq(text)
      end
    end

    context "when there is corresponding uploaded gallery in article" do
      it "replaces image tag with html code" do
        gallery = create(:oni_sorceress_content_image_gallery, :with_images, name: "quux")
        text = "foo {{gallery:quux}} bar"

        result = described_class.convert(text:, galleries: [gallery])
        expect(result.gsub(%r{\/rails.*?\.jpg}, "<url>")).to match_snapshot("gallery_component")
      end
    end
  end
end
