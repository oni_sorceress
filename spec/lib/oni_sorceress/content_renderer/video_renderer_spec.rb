# frozen_string_literal: true

describe OniSorceress::ContentRenderer::VideoRenderer do
  describe ".convert" do
    context "when video not uploaded to article" do
      it "does not replace video tag with html code" do
        text = "foo {{video:bbb.mp4}} bar"

        expect(described_class.convert(text:, videos: [])).to eq(text)
      end
    end

    context "when there is corresponding uploaded video in article" do
      it "replaces video tag with html code" do
        video = create(:oni_sorceress_content_video)
        text = "foo {{video:kingofthenorth.mp4;;title:King of The North}} bar"

        result = described_class.convert(text:, videos: [video])
        expect(result.gsub(%r{\/rails.*?\.mp4}, "<url>")).to match_snapshot("video_component")
      end
    end
  end
end
