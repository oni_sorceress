# frozen_string_literal: true

require "fileutils"

RSpec.describe OniSorceress::FilesystemImporter::ModelImporters::Article do
  let(:content_path) { Rails.root.join("tmp/test/content/") }
  let(:articles_path) { Rails.root.join("tmp/test/content/articles/Solar System/Planets") }
  let(:article) do
    <<~ARTICLE
      posted: 2077-12-10
      poster: John Snow
      language: en
      language: sk
      license: bootleg

      # merkur is the first planet in solar system

      blah blah
    ARTICLE
  end

  before do
    basepath = File.join(articles_path, "merkur")
    images_basepath = File.join(basepath, "images")
    image_galleries_basepath = File.join(basepath, "image_galleries")
    FileUtils.mkdir_p(basepath)
    FileUtils.mkdir_p(images_basepath)
    FileUtils.mkdir_p(image_galleries_basepath)

    File.write(File.join(basepath, "merkur.md"), article)
    FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), File.join(basepath, "images"))
  end

  after do
    FileUtils.rm_rf(articles_path)
  end

  it "imports already imported article" do
    world = create(:oni_sorceress_content_world, name: "root")
    language_sk = create(:oni_sorceress_content_language, world:, code: "sk")
    language_en = create(:oni_sorceress_content_language, world:, code: "en")

    article = create(
      :oni_sorceress_content_article,
      pretty_id: "merkur",
      images: [
        build(:oni_sorceress_content_image, file: Rails.root.join("spec/fixtures/images/meadow.jpg")),
      ],
      world:,
    )
    article.articles_languages << [
      build(:oni_sorceress_content_articles_language, language: language_sk, priority: 0),
      build(:oni_sorceress_content_articles_language, language: language_en, priority: 1),
    ]

    described_class.call(root_path: content_path)

    imported = OniSorceress::Content::Article.first

    expect(imported.attributes).to include({
      "hidden" => false,
      "last_updated_at" => Time.utc(2077, 12, 10),
      "original_filename" => include("content/articles/Solar System/Planets/merkur/merkur.md"),
      "pretty_id" => "merkur",
      "title" => "merkur is the first planet in solar system",
      "title_mime" => "text/markdown",
      "secret" => nil,
      "uri" => "articles:merkur",
    })

    expect(imported.author.name).to eq("John Snow")
    expect(imported.author.world.name).to eq("root")
    expect(imported.world.name).to eq("root")

    expect(imported.category.attributes).to include({
      "first_level" => "Solar System",
      "second_level" => "Planets",
    })
    expect(imported.category.world.name).to eq("root")

    expect(imported.license.attributes).to include({
      "abbr" => "bootleg",
      "name" => "bootleg",
      "link" => nil,
    })
    expect(imported.license.world.name).to eq("root")

    expect(imported.articles_languages.map { |item| item.slice(:language, :priority) }).to match_array([
      {
        language: language_en,
        priority: 0,
      },
      {
        language: language_sk,
        priority: 1,
      },
    ])

    expect(imported.images.map { |image| image.file.filename.to_s }).to contain_exactly("cherry_blossoms.jpg")
  end
end
