# frozen_string_literal: true

require "rails_helper"
require "fileutils"

RSpec.describe OniSorceress::FilesystemImporter::ModelImporters::Article do
  let(:content_path) { Rails.root.join("tmp/test/content/") }
  let(:articles_path) { Rails.root.join("tmp/test/content/articles/Solar System/Planets") }
  let(:article) do
    <<~ARTICLE
      posted: 2077-12-10
      poster: John Snow
      language: en
      language: sk
      license: bootleg

      # merkur is the first planet in solar system

      blah blah
    ARTICLE
  end

  before do
    basepath = File.join(articles_path, "merkur")
    images_basepath = File.join(basepath, "images")
    image_gallery_basepath = File.join(basepath, "image_galleries", "Photo Book")
    FileUtils.mkdir_p(basepath)
    FileUtils.mkdir_p(images_basepath)
    FileUtils.mkdir_p(image_gallery_basepath)

    File.write(File.join(basepath, "merkur.md"), article)
    FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), images_basepath)
    FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), image_gallery_basepath)
    FileUtils.cp(Rails.root.join("spec/fixtures/images/meadow.jpg"), image_gallery_basepath)
  end

  after do
    FileUtils.rm_rf(articles_path)
  end

  it "imports a new article" do
    described_class.call(root_path: content_path)

    imported = OniSorceress::Content::Article.first

    expect(imported.attributes).to include({
      "hidden" => false,
      "last_updated_at" => Time.utc(2077, 12, 10),
      "original_filename" => include("content/articles/Solar System/Planets/merkur/merkur.md"),
      "pretty_id" => "merkur",
      "title" => "merkur is the first planet in solar system",
      "title_mime" => "text/markdown",
      "secret" => nil,
      "uri" => "articles:merkur",
    })

    expect(imported.author.name).to eq("John Snow")
    expect(imported.author.world.name).to eq("root")
    expect(imported.world.name).to eq("root")

    expect(imported.category.attributes).to include({
      "first_level" => "Solar System",
      "second_level" => "Planets",
    })
    expect(imported.category.world.name).to eq("root")

    expect(imported.license.attributes).to include({
      "abbr" => "bootleg",
      "name" => "bootleg",
      "link" => nil,
    })
    expect(imported.license.world.name).to eq("root")

    expect(imported.languages.map(&:code)).to eq(["en", "sk"])
    expect(imported.articles_languages.map(&:priority)).to eq([0, 1])

    expect(imported.images.map { |image| image.file.filename.to_s }).to contain_exactly("cherry_blossoms.jpg")

    expect(imported.image_galleries.count).to eq(1)
    gallery = imported.image_galleries.first
    expect(gallery.name).to eq("Photo Book")

    expect(gallery.images.count).to eq(2)
    expect(gallery.images.map { |image| image.file.filename.to_s }).to contain_exactly("cherry_blossoms.jpg", "meadow.jpg")
  end

  context "when CC4-BY-SA" do
    let(:article) do
      <<~ARTICLE
        posted: 2077-12-10
        poster: John Snow
        language: en
        license: CC4-BY-SA

        # merkur is the first planet in solar system

        blah blah
      ARTICLE
    end

    it "sets name and link" do
      described_class.call(root_path: content_path)

      imported = OniSorceress::Content::Article.first
      expect(imported.license.abbr).to eq("CC4-BY-SA")
      expect(imported.license.name).to eq("Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)")
      expect(imported.license.link).to eq("https://creativecommons.org/licenses/by-sa/4.0/")
    end
  end

  context "when CC4.0 BY" do
    let(:article) do
      <<~ARTICLE
        posted: 2077-12-10
        poster: John Snow
        language: en
        license: CC4.0 BY

        # merkur is the first planet in solar system

        blah blah
      ARTICLE
    end

    it "sets name and link" do
      described_class.call(root_path: content_path)

      imported = OniSorceress::Content::Article.first
      expect(imported.license.abbr).to eq("CC4.0 BY")
      expect(imported.license.name).to eq("Attribution 4.0 International (CC BY 4.0)")
      expect(imported.license.link).to eq("https://creativecommons.org/licenses/by/4.0/")
    end
  end
end
