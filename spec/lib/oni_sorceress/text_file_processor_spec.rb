# frozen_string_literal: true

require_relative "../../../app/lib/oni_sorceress/text_file_processor"

describe OniSorceress::TextFileProcessor do
  describe ".world" do
    context "when in no world" do
      it "returns nil" do
        expect(described_class.world(
          base_path: "/var/log/",
          url: "/var/log/articles/foo/bar/quux.md",
        )).to be(nil)
      end
    end

    context "when in world" do
      it "returns world" do
        expect(described_class.world(
          base_path: "/var/log/",
          url: "/var/log/my_world/articles/foo/bar/quux.md",
        )).to eq("my_world")
      end
    end
  end

  describe ".first_level_category" do
    context "when in no world" do
      it "returns category" do
        expect(described_class.first_level_category(
          base_path: "/var/log/",
          url: "/var/log/articles/foo/bar/quux.md",
        )).to eq("foo")
      end
    end

    context "when in world" do
      it "returns category" do
        expect(described_class.first_level_category(
          base_path: "/var/log/",
          url: "/var/log/my_world/articles/foo/bar/quux.md",
        )).to eq("foo")
      end
    end
  end

  describe ".second_level_category" do
    context "when in no world" do
      it "returns category" do
        expect(described_class.second_level_category(
          base_path: "/var/log/",
          url: "/var/log/articles/foo/bar/quux.md",
        )).to eq("bar")
      end
    end

    context "when in world" do
      it "returns category" do
        expect(described_class.second_level_category(
          base_path: "/var/log/",
          url: "/var/log/my_world/articles/foo/bar/quux.md",
        )).to eq("bar")
      end
    end
  end

  describe ".title" do
    it "returns nil when text is empty" do
      expect(described_class.title(text: nil)).to be(nil)
      expect(described_class.title(text: "")).to be(nil)
      expect(described_class.title(text: " ")).to be(nil)
    end

    it "ignores whitespace at the start of a text" do
      expect(described_class.title(text: "     # aaa")).to eq("aaa")
    end

    it "returns markdown title" do
      expect(described_class.title(text: "# foo: bar")).to eq("foo: bar")
    end

    it "returns HTML title" do
      expect(described_class.title(text: "<h1>foo: bar</h1>")).to eq("foo: bar")
    end

    it "returns Textile title" do
      expect(described_class.title(text: "h1.foo: bar")).to eq("foo: bar")
    end
  end

  # TODO: validate the format of metadata
  describe ".split_metadata_and_content" do
    it "splits text to metadata and body" do
      expect(described_class.split_metadata_and_content(text: "foo: bar\n\nsome body")).to eq([
        { "foo" => ["bar"] },
        "some body",
      ])
    end

    it "handles no headers" do
      expect(described_class.split_metadata_and_content(text: "some body")).to eq([
        {},
        "some body",
      ])
    end

    it "removes whitespaces from body from the left side" do
      expect(described_class.split_metadata_and_content(text: "     \nsome body  \n")).to eq([
        {},
        "some body  \n",
      ])
    end
  end

  describe ".deserialize_metadata" do
    it "joins the same metadata headers into an array" do
      file = "foo: bar\nfoo: quux"
      expect(described_class.deserialize_metadata(text: file)).to eq({
        "foo" => ["bar", "quux"],
      })
    end

    it "handles space and no space in key:value" do
      file = "foo: bar\nfoo:quux"
      expect(described_class.deserialize_metadata(text: file)).to eq({
        "foo" => ["bar", "quux"],
      })
    end

    it "handles multiple colons" do
      file = "foo: my:value"
      expect(described_class.deserialize_metadata(text: file)).to eq({
        "foo" => ["my:value"],
      })
    end

    it "handles multiple keys" do
      file = "foo: bar\nquux: baz"
      expect(described_class.deserialize_metadata(text: file)).to eq({
        "foo" => ["bar"],
        "quux" => ["baz"],
      })
    end
  end
end
