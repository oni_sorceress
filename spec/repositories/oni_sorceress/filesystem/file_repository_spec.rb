# frozen_string_literal: true

require "fileutils"

RSpec.describe OniSorceress::Filesystem::FileRepository do
  before do
    FileUtils.mkdir_p(categories_path)
  end

  after do
    FileUtils.rm_rf(repository)
  end

  let(:repository) { Rails.root.join("tmp/test/content/") }
  let(:articles_path) { repository.join("articles") }
  let(:categories_path) { articles_path.join("Solar System/Planets") }

  describe ".all" do
    it "finds all models from files" do
      File.write(File.join(categories_path, "foo.md"), "test file")
      File.write(File.join(categories_path, "bar.md"), "test file2")

      files = described_class.all(model: "articles", dir: repository)
      expect(files.map { |f| f.dig(:headers, :uri) }).to contain_exactly("articles:foo", "articles:bar")
    end

    it "can filter files by extensions" do
      File.write(File.join(categories_path, "foo.md"), "test file")
      File.write(File.join(categories_path, "bar.txt"), "test file2")
      File.write(File.join(categories_path, "quux.html"), "test file3")

      files = described_class.all(model: "articles", dir: repository, file_formats: ["md", "html"])
      expect(files.map { |f| f.dig(:headers, :uri) }).to contain_exactly("articles:foo", "articles:quux")
    end

    it "loads the body by default" do
      File.write(File.join(categories_path, "foo.md"), "test file")

      files = described_class.all(model: "articles", dir: repository)
      expect(files.first[:body]).to eq("test file")
    end

    it "doesn't read the body when load flag is false" do
      File.write(File.join(categories_path, "foo.md"), "test file")

      files = described_class.all(model: "articles", dir: repository, load: false)
      expect(files.first[:body]).to be(nil)
    end

    it "adds metadata to files" do
      subnested_article_path = File.join(categories_path, "foo")
      FileUtils.mkdir_p(subnested_article_path)
      File.write(File.join(subnested_article_path, "foo.md"), "test file")

      files = described_class.all(model: "articles", dir: repository, load: false)

      headers = files.first[:headers]
      expect(headers[:path]).to eq("#{subnested_article_path}/foo.md")
      expect(headers[:base_dir]).to eq(subnested_article_path.to_s)
      expect(headers[:uri_id]).to eq("foo")
      expect(headers[:uri]).to eq("articles:foo")
    end
  end

  describe ".find" do
    it "finds the first model from files" do
      File.write(File.join(categories_path, "foo.md"), "test file")

      file = described_class.find(model: "articles", dir: repository, uri: "foo")
      expect(file.dig(:headers, :uri)).to eq("articles:foo")
    end

    it "returns nil when model cannot be found" do
      file = described_class.find(model: "articles", dir: repository, uri: "foo")
      expect(file).to be(nil)
    end

    it "can filter file by extensions" do
      File.write(File.join(categories_path, "foo.md"), "test file")
      File.write(File.join(categories_path, "foo.bin"), "test file2")

      file = described_class.find(model: "articles", dir: repository, uri: "foo", file_formats: ["md"])
      expect(file.dig(:headers, :path)).to include("foo.md")
    end

    it "loads the body by default" do
      File.write(File.join(categories_path, "foo.md"), "test file")

      file = described_class.find(model: "articles", dir: repository, uri: "foo")
      expect(file[:body]).to eq("test file")
    end

    it "doesn't read the body when load flag is false" do
      File.write(File.join(categories_path, "foo.md"), "test file")

      file = described_class.find(model: "articles", dir: repository, uri: "foo", load: false)
      expect(file[:body]).to be(nil)
    end

    it "adds metadata to files" do
      subnested_article_path = File.join(categories_path, "foo")
      FileUtils.mkdir_p(subnested_article_path)
      File.write(File.join(subnested_article_path, "foo.md"), "test file")

      file = described_class.find(model: "articles", dir: repository, uri: "foo", load: false)

      headers = file[:headers]
      expect(headers[:path]).to eq("#{subnested_article_path}/foo.md")
      expect(headers[:base_dir]).to eq(subnested_article_path.to_s)
      expect(headers[:uri_id]).to eq("foo")
      expect(headers[:uri]).to eq("articles:foo")
    end

    it "loads images" do
      subnested_article_path = File.join(categories_path, "foo")
      images_path = File.join(subnested_article_path, "images")
      FileUtils.mkdir_p(File.join(images_path))
      File.write(File.join(subnested_article_path, "foo.md"), "test file")
      FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), images_path)

      file = described_class.find(model: "articles", dir: repository, uri: "foo", load: false)
      expect(file[:image_filenames]).to contain_exactly(File.join(images_path, "cherry_blossoms.jpg"))
    end

    it "loads image galleries" do
      subnested_article_path = File.join(categories_path, "foo")
      image_galleries_path1 = File.join(subnested_article_path, "image_galleries/Photos of Nature")
      image_galleries_path2 = File.join(subnested_article_path, "image_galleries/Random Photos")
      FileUtils.mkdir_p(File.join(image_galleries_path1))
      FileUtils.mkdir_p(File.join(image_galleries_path2))
      File.write(File.join(subnested_article_path, "foo.md"), "test file")
      FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), image_galleries_path1)
      FileUtils.cp(Rails.root.join("spec/fixtures/images/meadow.jpg"), image_galleries_path1)
      FileUtils.cp(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), image_galleries_path2)

      file = described_class.find(model: "articles", dir: repository, uri: "foo", load: false)
      expect(file[:image_galleries]).to match_array([
        {
          name: "Photos of Nature",
          image_filenames: [
            File.join(image_galleries_path1, "cherry_blossoms.jpg").to_s,
            File.join(image_galleries_path1, "meadow.jpg").to_s,
          ],
        },
        {
          name: "Random Photos",
          image_filenames: [
            File.join(image_galleries_path2, "cherry_blossoms.jpg").to_s,
          ],
        },
      ])
    end

    it "loads videos" do
      subnested_article_path = File.join(categories_path, "foo")
      videos_path = File.join(subnested_article_path, "videos")
      FileUtils.mkdir_p(File.join(videos_path))
      File.write(File.join(subnested_article_path, "foo.md"), "test file")
      FileUtils.cp(Rails.root.join("spec/fixtures/videos/kingofthenorth.mp4"), videos_path)

      file = described_class.find(model: "articles", dir: repository, uri: "foo", load: false)
      expect(file[:video_filenames]).to contain_exactly(File.join(videos_path, "kingofthenorth.mp4"))
    end
  end
end
