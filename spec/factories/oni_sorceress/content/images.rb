# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_images
#
#  id                                   :uuid             not null, primary key
#  oni_sorceress_content_imageable_type :string           not null
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  oni_sorceress_content_imageable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_imageable  (oni_sorceress_content_imageable_type,oni_sorceress_content_imageable_id)
#
FactoryBot.define do
  factory :oni_sorceress_content_image, class: "OniSorceress::Content::Image" do
    oni_sorceress_content_imageable factory: [:oni_sorceress_content_article]
    file { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/images/cherry_blossoms.jpg"), "image/jpeg") }
  end
end
