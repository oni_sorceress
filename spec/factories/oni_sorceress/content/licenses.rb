# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_licenses
#
#  id                             :uuid             not null, primary key
#  abbr                           :text             not null
#  link                           :text
#  name                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_abbr_66a0375cc7  (oni_sorceress_content_world_id,abbr) UNIQUE
#  index_oni_sorceress_content_licenses_on_name           (name) UNIQUE
#  index_oni_sorceress_content_licenses_on_world_id       (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_28f0dc90ae  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
FactoryBot.define do
  factory :oni_sorceress_content_license, class: "::OniSorceress::Content::License" do
    world factory: [:oni_sorceress_content_world]
    abbr { FFaker::Lorem.words(2).join("-") }
    link { "???" }
    name { FFaker::Lorem.words(2).join("-") }
  end
end
