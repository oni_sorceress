# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_comments
#
#  id                                     :uuid             not null, primary key
#  body                                   :text             not null
#  oni_sorceress_content_commentable_type :string           not null
#  poster_name                            :text
#  published_at                           :datetime
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_content_commentable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_comment_on_commentable  (oni_sorceress_content_commentable_type,oni_sorceress_content_commentable_id)
#
FactoryBot.define do
  factory :oni_sorceress_content_comment, class: "::OniSorceress::Content::Comment" do
    oni_sorceress_content_commentable factory: [:oni_sorceress_content_article]
    request factory: [:oni_sorceress_analytics_request]
    body { "MyText" }
    poster_name { "MyText" }
    published_at { nil }
  end
end
