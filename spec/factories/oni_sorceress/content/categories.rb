# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_categories
#
#  id                             :uuid             not null, primary key
#  first_level                    :text             not null
#  second_level                   :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_categories_on_first_second_world  (first_level,second_level,oni_sorceress_content_world_id) UNIQUE
#  index_oni_sorceress_content_categories_on_world_id            (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_90d1cbdda4  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
FactoryBot.define do
  factory :oni_sorceress_content_category, class: "::OniSorceress::Content::Category" do
    world factory: [:oni_sorceress_content_world]
    first_level { FFaker::Lorem.words(2).join(" ") }
    second_level { FFaker::Lorem.words(2).join(" ") }
  end
end
