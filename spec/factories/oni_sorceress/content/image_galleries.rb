# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_image_galleries
#
#  id                                           :uuid             not null, primary key
#  name                                         :text
#  oni_sorceress_content_image_galleryable_type :string           not null
#  position                                     :integer          default(9999)
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  oni_sorceress_content_image_galleryable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_image_galleryable  (oni_sorceress_content_image_galleryable_type,oni_sorceress_content_image_galleryable_id)
#
FactoryBot.define do
  factory :oni_sorceress_content_image_gallery, class: "OniSorceress::Content::ImageGallery" do
    oni_sorceress_content_image_galleryable factory: :oni_sorceress_content_article

    trait :with_images do
      images { [build(:oni_sorceress_content_image), build(:oni_sorceress_content_image)] }
    end
  end
end
