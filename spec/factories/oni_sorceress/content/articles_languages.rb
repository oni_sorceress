# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_articles_languages
#
#  id                                :uuid             not null, primary key
#  priority                          :integer          default(1), not null
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  oni_sorceress_content_article_id  :uuid             not null
#  oni_sorceress_content_language_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_article_id_oni_sorcere_327f950961  (oni_sorceress_content_article_id,oni_sorceress_content_language_id) UNIQUE
#  index_oni_sorceress_content_art_lang_on_article_id              (oni_sorceress_content_article_id)
#  index_oni_sorceress_content_art_lang_on_language_id             (oni_sorceress_content_language_id)
#
# Foreign Keys
#
#  fk_rails_59879288e1  (oni_sorceress_content_article_id => oni_sorceress_content_articles.id)
#  fk_rails_a8fb0dab09  (oni_sorceress_content_language_id => oni_sorceress_content_languages.id)
#
FactoryBot.define do
  factory :oni_sorceress_content_articles_language, class: "::OniSorceress::Content::ArticlesLanguage" do
    article factory: :oni_sorceress_content_article
    language factory: :oni_sorceress_content_language
  end
end
