# frozen_string_literal: true

FactoryBot.define do
  factory :oni_sorceress_content_video, class: "OniSorceress::Content::Video" do
    oni_sorceress_content_videoable factory: [:oni_sorceress_content_article]
    file { Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/videos/kingofthenorth.mp4"), "video/mp4") }
  end
end
