# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_languages
#
#  id                             :uuid             not null, primary key
#  code                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_code_2114b11a50  (oni_sorceress_content_world_id,code) UNIQUE
#  index_oni_sorceress_content_languages_on_world_id      (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_4d923847dd  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
FactoryBot.define do
  factory :oni_sorceress_content_language, class: "::OniSorceress::Content::Language" do
    code { FFaker::Lorem.word }
    world factory: [:oni_sorceress_content_world]
  end
end
