# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_articles
#
#  id                                :uuid             not null, primary key
#  body                              :text
#  body_mime                         :text
#  hidden                            :boolean          default(FALSE), not null
#  last_updated_at                   :datetime
#  original_filename                 :text
#  posted_at                         :datetime
#  secret                            :text
#  title                             :text
#  title_mime                        :text
#  uri                               :text
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  oni_sorceress_content_author_id   :uuid
#  oni_sorceress_content_category_id :uuid
#  oni_sorceress_content_license_id  :uuid
#  oni_sorceress_content_world_id    :uuid
#  pretty_id                         :text             not null
#
# Indexes
#
#  index_oni_sorceress_content_articles_on_author_id    (oni_sorceress_content_author_id)
#  index_oni_sorceress_content_articles_on_category_id  (oni_sorceress_content_category_id)
#  index_oni_sorceress_content_articles_on_license_id   (oni_sorceress_content_license_id)
#  index_oni_sorceress_content_articles_on_pretty_id    (pretty_id) UNIQUE
#  index_oni_sorceress_content_articles_on_world_id     (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_8c982eeadb  (oni_sorceress_content_author_id => oni_sorceress_content_authors.id)
#  fk_rails_a476655b71  (oni_sorceress_content_license_id => oni_sorceress_content_licenses.id)
#  fk_rails_c481d88894  (oni_sorceress_content_category_id => oni_sorceress_content_categories.id)
#  fk_rails_de2f5c8f1e  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
FactoryBot.define do
  factory :oni_sorceress_content_article, class: "::OniSorceress::Content::Article" do
    after(:build) do |article, _evaluator|
      if !article.author
        article.author = build(:oni_sorceress_content_author, world: article.world)
      end

      if !article.category
        article.category = build(:oni_sorceress_content_category, world: article.world)
      end

      if !article.license
        article.license = build(:oni_sorceress_content_license, world: article.world)
      end
    end

    author factory: [:oni_sorceress_content_author], strategy: :null
    category factory: [:oni_sorceress_content_category], strategy: :null
    license factory: [:oni_sorceress_content_license], strategy: :null
    world { OniSorceress::Content::World.find_by(name: "root") || build(:oni_sorceress_content_world, name: "root") }

    body { FFaker::Lorem.words(10).join(" ") }
    original_filename { "file.md" }
    posted_at { 10.minutes.ago }
    last_updated_at { 5.minutes.ago }
    pretty_id { SecureRandom.uuid }

    title { "Foo" }

    trait :with_image do
      after :create do |article|
        image = build(:oni_sorceress_content_image)
        article.images << image
        article.update!(body: (article.body += "\n\n{{image:#{image.file.filename};;title:Example title of image}}\n"))
      end
    end

    trait :with_gallery do
      after :create do |article|
        article.image_galleries << build(:oni_sorceress_content_image_gallery, :with_images, name: "Example Gallery")
        article.update!(body: (article.body += "\n\n{{gallery:Example Gallery}}\n"))
      end
    end
  end
end
