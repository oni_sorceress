# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_worlds
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_oni_sorceress_content_worlds_on_name  (name) UNIQUE
#
FactoryBot.define do
  factory :oni_sorceress_content_world, class: "::OniSorceress::Content::World" do
    name { FFaker::Lorem.words(2).join("-") }
  end
end
