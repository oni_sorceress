# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_bogofilter_hams
#
#  id                                     :uuid             not null, primary key
#  learn_state                            :integer          default("not_learned"), not null
#  oni_sorceress_bogofilter_hammable_type :string           not null
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_bogofilter_hammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_hams_on_hammable  (oni_sorceress_bogofilter_hammable_type,oni_sorceress_bogofilter_hammable_id)
#
FactoryBot.define do
  factory :oni_sorceress_analytics_bogofilter_ham, class: "::OniSorceress::Analytics::BogofilterHam" do
    oni_sorceress_bogofilter_hammable factory: [:oni_sorceress_analytics_spam_check]
  end
end
