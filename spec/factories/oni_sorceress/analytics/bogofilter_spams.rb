# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_bogofilter_spams
#
#  id                                      :uuid             not null, primary key
#  learn_state                             :integer          default("not_learned"), not null
#  oni_sorceress_bogofilter_spammable_type :string           not null
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  oni_sorceress_bogofilter_spammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_spams_on_spammable  (oni_sorceress_bogofilter_spammable_type,oni_sorceress_bogofilter_spammable_id)
#
FactoryBot.define do
  factory :oni_sorceress_analytics_bogofilter_spam, class: "::OniSorceress::Analytics::BogofilterSpam" do
    oni_sorceress_bogofilter_spammable factory: [:oni_sorceress_analytics_spam_check]
  end
end
