# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_spam_checks
#
#  id                           :uuid             not null, primary key
#  classifier                   :integer          not null
#  oni_sorceress_spammable_type :string           not null
#  result                       :integer          not null
#  score                        :float            not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  oni_sorceress_spammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_spam_checks_on_spammable  (oni_sorceress_spammable_type,oni_sorceress_spammable_id)
#
FactoryBot.define do
  factory :oni_sorceress_analytics_spam_check, class: "::OniSorceress::Analytics::SpamCheck" do
    score { 85 }
    classifier { :manual }
    result { :ham }
    oni_sorceress_spammable factory: [:oni_sorceress_content_article]
  end
end
