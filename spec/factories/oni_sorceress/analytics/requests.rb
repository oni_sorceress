# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_requests
#
#  id                               :uuid             not null, primary key
#  application_server_request_end   :datetime
#  application_server_request_start :datetime
#  data                             :jsonb
#  oni_sorceress_requestable_type   :string
#  status_code                      :integer
#  uid                              :text
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  oni_sorceress_requestable_id     :uuid
#
# Indexes
#
#  index_oni_sorceress_analytics_requests_on_requestable  (oni_sorceress_requestable_type,oni_sorceress_requestable_id)
#
FactoryBot.define do
  factory :oni_sorceress_analytics_request, class: "::OniSorceress::Analytics::Request" do
    data { "" }
    status_code { 1 }
    application_server_request_start { "2022-03-19 16:16:39" }
    application_server_request_end { "2022-03-19 16:16:39" }
  end
end
