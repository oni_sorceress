# frozen_string_literal: true

describe OniSorceress::Filesystem::ArticleDecorator do
  let(:article) do
    OniSorceress::Filesystem::Article.new(file: {
      headers: {
        path: "/var/lib/articles/gret.md",
        base_dir: "/var/lib/articles/",
        content_dir: "/var/lib",
        uri: "article:why-i-m-so-gret",
      },
      body: <<~BODY,
        posted: 2019-10-28

        # Why I'm so great

        Since my childhood....
      BODY
    })
  end

  describe ".call" do
    it "renders the body" do
      pending "fix"
      expect(described_class.call(article)[:rendered]).to eq(
        "<h1 id=\"why-im-so-great\">Why I’m so great</h1>\n\n<p>Since my childhood….</p>\n",
      )
    end

    it "extracts a title" do
      expect(described_class.call(article)[:title]).to eq(
        "Why I'm so great",
      )
    end

    it "provides metadata" do
      expect(described_class.call(article)[:metadata]).to eq(
        { "posted" => ["2019-10-28"] },
      )
    end
  end
end
