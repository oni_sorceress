# frozen_string_literal: true

require "rails_helper"

# rubocop:disable RSpec/DescribeClass
RSpec.describe "factories" do
  it "lints factories" do
    expect(FactoryBot.lint(traits: true)).to be(nil)
  end
end
# rubocop:enable RSpec/DescribeClass
