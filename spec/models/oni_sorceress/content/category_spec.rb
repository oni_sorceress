# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_categories
#
#  id                             :uuid             not null, primary key
#  first_level                    :text             not null
#  second_level                   :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_categories_on_first_second_world  (first_level,second_level,oni_sorceress_content_world_id) UNIQUE
#  index_oni_sorceress_content_categories_on_world_id            (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_90d1cbdda4  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
RSpec.describe OniSorceress::Content::Category do
  describe "associations" do
    it { is_expected.to belong_to(:world) }
  end

  describe "validations" do
    subject { build(:oni_sorceress_content_category) }

    it { is_expected.to validate_presence_of(:first_level) }
    it { is_expected.to validate_presence_of(:second_level) }
    it { is_expected.to validate_uniqueness_of(:second_level).scoped_to(:first_level, :oni_sorceress_content_world_id) }
  end

  describe "scopes" do
    describe ".in_world" do
      it "gets categories belonging to specific world sorted by name" do
        world = create(:oni_sorceress_content_world, name: "foo")
        cat1 = create(:oni_sorceress_content_category, first_level: "bbb", world:)
        cat2 = create(:oni_sorceress_content_category, first_level: "aaa", second_level: "bbbb", world:)
        cat3 = create(:oni_sorceress_content_category, first_level: "ccc", world:)
        cat4 = create(:oni_sorceress_content_category, first_level: "aaa", second_level: "aaaa", world:)

        create(:oni_sorceress_content_article, category: cat1, world: world)
        create(:oni_sorceress_content_article, category: cat2, world: world)
        create(:oni_sorceress_content_article, category: cat3, world: world)
        create(:oni_sorceress_content_article, category: cat4, world: world)

        expect(described_class.in_world(world.name)).to eq([cat4, cat2, cat1, cat3])
      end

      it "does not get categories from different world" do
        world1 = create(:oni_sorceress_content_world)
        cat1 = create(:oni_sorceress_content_category, world: world1)
        create(:oni_sorceress_content_article, category: cat1, world: world1)

        world2 = create(:oni_sorceress_content_world)
        cat2 = create(:oni_sorceress_content_category, world: world2)
        create(:oni_sorceress_content_article, category: cat2, world: world2)

        expect(described_class.in_world(world1.name)).to eq([cat1])
      end

      it "does not get categories with no articles" do
        world = create(:oni_sorceress_content_world)
        create(:oni_sorceress_content_category, world: world)

        expect(described_class.in_world(world.name)).to eq([])
      end

      it "does not get categories with only hidden articles" do
        world = create(:oni_sorceress_content_world)
        cat = create(:oni_sorceress_content_category, world: world)
        create(:oni_sorceress_content_article, category: cat, world: world, hidden: true)

        expect(described_class.in_world(world.name)).to eq([])
      end
    end

    describe ".in_world_first_level" do
      it "gets unique first level categories sorted by name" do
        world = create(:oni_sorceress_content_world, name: "foo")
        cat1 = create(:oni_sorceress_content_category, first_level: "bbb", world:)
        cat2 = create(:oni_sorceress_content_category, first_level: "aaa", world:)
        cat3 = create(:oni_sorceress_content_category, first_level: "ccc", world:)
        cat4 = create(:oni_sorceress_content_category, first_level: "aaa", world:)

        create(:oni_sorceress_content_article, category: cat1, world: world)
        create(:oni_sorceress_content_article, category: cat2, world: world)
        create(:oni_sorceress_content_article, category: cat3, world: world)
        create(:oni_sorceress_content_article, category: cat4, world: world)

        expect(described_class.in_world_first_level(world.name)).to eq(["aaa", "bbb", "ccc"])
      end

      it "does not get categories from different world" do
        world1 = create(:oni_sorceress_content_world, name: "foo")
        cat1 = create(:oni_sorceress_content_category, first_level: "aaa", world: world1)
        create(:oni_sorceress_content_article, category: cat1, world: world1)

        world2 = create(:oni_sorceress_content_world, name: "bar")
        cat2 = create(:oni_sorceress_content_category, first_level: "bbb", world: world2)
        create(:oni_sorceress_content_article, category: cat2, world: world2)

        expect(described_class.in_world_first_level(world1.name)).to eq(["aaa"])
      end

      it "does not get categories with no articles" do
        world = create(:oni_sorceress_content_world)
        create(:oni_sorceress_content_category, world: world)

        expect(described_class.in_world_first_level(world.name)).to eq([])
      end

      it "does not get categories with only hidden articles" do
        world = create(:oni_sorceress_content_world)
        cat = create(:oni_sorceress_content_category, world: world)
        create(:oni_sorceress_content_article, category: cat, world: world, hidden: true)

        expect(described_class.in_world_first_level(world.name)).to eq([])
      end
    end
  end
end
