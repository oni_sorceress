# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_articles
#
#  id                                :uuid             not null, primary key
#  body                              :text
#  body_mime                         :text
#  hidden                            :boolean          default(FALSE), not null
#  last_updated_at                   :datetime
#  original_filename                 :text
#  posted_at                         :datetime
#  secret                            :text
#  title                             :text
#  title_mime                        :text
#  uri                               :text
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  oni_sorceress_content_author_id   :uuid
#  oni_sorceress_content_category_id :uuid
#  oni_sorceress_content_license_id  :uuid
#  oni_sorceress_content_world_id    :uuid
#  pretty_id                         :text             not null
#
# Indexes
#
#  index_oni_sorceress_content_articles_on_author_id    (oni_sorceress_content_author_id)
#  index_oni_sorceress_content_articles_on_category_id  (oni_sorceress_content_category_id)
#  index_oni_sorceress_content_articles_on_license_id   (oni_sorceress_content_license_id)
#  index_oni_sorceress_content_articles_on_pretty_id    (pretty_id) UNIQUE
#  index_oni_sorceress_content_articles_on_world_id     (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_8c982eeadb  (oni_sorceress_content_author_id => oni_sorceress_content_authors.id)
#  fk_rails_a476655b71  (oni_sorceress_content_license_id => oni_sorceress_content_licenses.id)
#  fk_rails_c481d88894  (oni_sorceress_content_category_id => oni_sorceress_content_categories.id)
#  fk_rails_de2f5c8f1e  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
RSpec.describe OniSorceress::Content::Article do
  describe "typical example" do
    it "instances a new article" do
      world = OniSorceress::Content::World.create!(name: "kpop")
      author = OniSorceress::Content::Author.create!(name: "Fierce Eyes", world:)
      category = OniSorceress::Content::Category.create!(first_level: "concerts", second_level: "2022", world:)
      license = OniSorceress::Content::License.create!(abbr: "CC4-BY", name: "Creative Commons 4.0 Attribution", world:)

      article = described_class.create!(
        author:,
        body: "Following concerts available:",
        body_mime: "text/markdown",
        category:,
        hidden: true,
        last_updated_at: Time.zone.local(2022, 5, 2),
        license:,
        original_filename: "bts-tour.md",
        posted_at: Time.zone.local(2022, 5, 1),
        pretty_id: "bts-tour",
        secret: "topsecret",
        title: "BTS Tour",
        title_mime: "text/markdown",
        uri: "articles/music/kpop/bts-tour.md",
        world:,
      )

      expect(article.reload.id).to match(/\A\w{8}/)
      expect(article.world.name).to eq("kpop")
      expect(article.author.name).to eq("Fierce Eyes")
      expect(article.category.first_level).to eq("concerts")
      expect(article.license.abbr).to eq("CC4-BY")
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:pretty_id) }
    it { is_expected.to validate_presence_of(:original_filename) }
    it { is_expected.to validate_presence_of(:title) }

    it "requires category, license and author to belong to the same world" do
      world = create(:oni_sorceress_content_world, name: "kpop")
      author = create(:oni_sorceress_content_author, name: "Fierce Eyes", world:)
      category = create(:oni_sorceress_content_category, first_level: "concerts", second_level: "2022", world:)
      license = create(:oni_sorceress_content_license, abbr: "CC4-BY", name: "Creative Commons 4.0 Attribution", world:)

      another_world = create(:oni_sorceress_content_world)
      article = build(:oni_sorceress_content_article, author:, category:, license:, world: another_world)
      article.save

      expect(article.errors.to_hash).to eq({
        author: ["must belong to the same world"],
        category: ["must belong to the same world"],
        license: ["must belong to the same world"],
      })
    end

    it "requires lanugages to long to the same world" do
      world = create(:oni_sorceress_content_world, name: "kpop")
      author = create(:oni_sorceress_content_author, name: "Fierce Eyes", world:)
      category = create(:oni_sorceress_content_category, first_level: "concerts", second_level: "2022", world:)
      license = create(:oni_sorceress_content_license, abbr: "CC4-BY", name: "Creative Commons 4.0 Attribution", world:)

      article = create(:oni_sorceress_content_article, author:, category:, license:, world:)

      another_world = create(:oni_sorceress_content_world)
      language1 = create(:oni_sorceress_content_language, world:)
      language2 = create(:oni_sorceress_content_language, world: another_world)

      article.languages = [language1, language2]
      article.save

      expect(article.errors.to_hash).to eq({
        language: ["all must belong to the same world"],
      })
    end
  end

  describe "associations" do
    it "has comments" do
      article = create(:oni_sorceress_content_article)
      expect { article.comments << build(:oni_sorceress_content_comment) }.to change { article.comments.count }.by(1)
    end

    it "has images" do
      article = create(:oni_sorceress_content_article)
      expect { article.images << build(:oni_sorceress_content_image) }.to change { article.images.count }.by(1)
    end

    it "has image galleries" do
      article = create(:oni_sorceress_content_article)
      expect { article.image_galleries << build(:oni_sorceress_content_image_gallery) }.to change { article.image_galleries.count }.by(1)
    end
  end
end
