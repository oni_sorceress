# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_languages
#
#  id                             :uuid             not null, primary key
#  code                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_code_2114b11a50  (oni_sorceress_content_world_id,code) UNIQUE
#  index_oni_sorceress_content_languages_on_world_id      (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_4d923847dd  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
RSpec.describe OniSorceress::Content::Language do
  describe "associations" do
    it { is_expected.to belong_to(:world) }
  end

  describe "validations" do
    subject { build(:oni_sorceress_content_language) }

    it { is_expected.to validate_presence_of(:code) }
    it { is_expected.to validate_uniqueness_of(:code).scoped_to(:oni_sorceress_content_world_id) }
  end
end
