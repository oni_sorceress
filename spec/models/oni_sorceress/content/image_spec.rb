# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_images
#
#  id                                   :uuid             not null, primary key
#  oni_sorceress_content_imageable_type :string           not null
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  oni_sorceress_content_imageable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_imageable  (oni_sorceress_content_imageable_type,oni_sorceress_content_imageable_id)
#
RSpec.describe OniSorceress::Content::Image do
  describe "associations" do
    it { is_expected.to belong_to(:oni_sorceress_content_imageable) }
  end

  describe "validations" do
    subject { build(:oni_sorceress_content_image) }

    it { is_expected.to validate_presence_of(:file) }
  end
end
