# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_worlds
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_oni_sorceress_content_worlds_on_name  (name) UNIQUE
#
RSpec.describe OniSorceress::Content::World do
  describe "validations" do
    subject { build(:oni_sorceress_content_world) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end
