# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_comments
#
#  id                                     :uuid             not null, primary key
#  body                                   :text             not null
#  oni_sorceress_content_commentable_type :string           not null
#  poster_name                            :text
#  published_at                           :datetime
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_content_commentable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_comment_on_commentable  (oni_sorceress_content_commentable_type,oni_sorceress_content_commentable_id)
#
RSpec.describe OniSorceress::Content::Comment do
  subject { build(:oni_sorceress_content_comment) }

  describe "associations" do
    it { is_expected.to belong_to(:oni_sorceress_content_commentable) }
    it { is_expected.to have_one(:request).dependent(:destroy) }
    it { is_expected.to have_many(:spam_checks).dependent(:destroy) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:request) }
    it { is_expected.to validate_length_of(:poster_name).is_at_most(100) }
    it { is_expected.to validate_length_of(:body).is_at_most(4000) }
  end

  describe "scopes" do
    describe ".unpublished" do
      it "returns unpublished comments" do
        unpublished_comment = create(:oni_sorceress_content_comment, published_at: nil)
        create(:oni_sorceress_content_comment, published_at: Time.zone.now)

        expect(described_class.unpublished).to contain_exactly(unpublished_comment)
      end
    end

    describe ".without_manual_spam_check" do
      it "returns comments without manual spam check" do
        comment_no_checks = create(:oni_sorceress_content_comment, spam_checks: [])
        comment_bogofilter = create(:oni_sorceress_content_comment, spam_checks: [build(:oni_sorceress_analytics_spam_check, classifier: :bogofilter)])
        create(:oni_sorceress_content_comment, spam_checks: [build(:oni_sorceress_analytics_spam_check, classifier: :manual)])

        expect(described_class.without_manual_spam_check).to match_array([comment_bogofilter, comment_no_checks])
      end
    end

    describe ".not_spam" do
      it "returns only ham and unsure comments" do
        pending "fix"

        no_spam_check_comment = create(:oni_sorceress_content_comment, body: "no spam check")
        create(:oni_sorceress_content_comment, body: "spam", spam_checks: [create(:oni_sorceress_analytics_spam_check, result: :spam)])
        ham_comment = create(:oni_sorceress_content_comment, body: "ham", spam_checks: [create(:oni_sorceress_analytics_spam_check, result: :ham)])
        unsure_comment = create(:oni_sorceress_content_comment, body: "unsure", spam_checks: [create(:oni_sorceress_analytics_spam_check, result: :unsure)])
        create(
          :oni_sorceress_content_comment,
          body: "manual spam",
          spam_checks: [
            create(:oni_sorceress_analytics_spam_check, classifier: :bogofilter, result: :unsure, score: 0.847015),
            create(:oni_sorceress_analytics_spam_check, classifier: :manual, result: :spam, score: 100),
          ],
        )
        expect(described_class.not_spam).to contain_exactly(no_spam_check_comment, ham_comment, unsure_comment)
      end
    end
  end
end
