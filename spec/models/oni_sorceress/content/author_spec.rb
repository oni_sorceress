# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_authors
#
#  id                             :uuid             not null, primary key
#  bio                            :text
#  name                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_name_c3b865a681  (oni_sorceress_content_world_id,name) UNIQUE
#  index_oni_sorceress_content_authors_on_world_id        (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_e47066d1b2  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
RSpec.describe OniSorceress::Content::Author do
  describe "associations" do
    it { is_expected.to belong_to(:world) }
  end

  describe "validations" do
    subject { build(:oni_sorceress_content_author) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:oni_sorceress_content_world_id) }
  end
end
