# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_spam_checks
#
#  id                           :uuid             not null, primary key
#  classifier                   :integer          not null
#  oni_sorceress_spammable_type :string           not null
#  result                       :integer          not null
#  score                        :float            not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  oni_sorceress_spammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_spam_checks_on_spammable  (oni_sorceress_spammable_type,oni_sorceress_spammable_id)
#
RSpec.describe OniSorceress::Analytics::SpamCheck do
  subject { build(:oni_sorceress_analytics_spam_check) }

  describe "associations" do
    it { is_expected.to belong_to(:oni_sorceress_spammable) }
  end

  it { is_expected.to define_enum_for(:classifier).with_values([:manual, :bogofilter]) }
  it { is_expected.to define_enum_for(:result).with_values([:unsure, :spam, :ham]) }
end
