# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_requests
#
#  id                               :uuid             not null, primary key
#  application_server_request_end   :datetime
#  application_server_request_start :datetime
#  data                             :jsonb
#  oni_sorceress_requestable_type   :string
#  status_code                      :integer
#  uid                              :text
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  oni_sorceress_requestable_id     :uuid
#
# Indexes
#
#  index_oni_sorceress_analytics_requests_on_requestable  (oni_sorceress_requestable_type,oni_sorceress_requestable_id)
#

RSpec.describe OniSorceress::Analytics::Request do
  subject { build(:oni_sorceress_analytics_request) }

  describe "associations" do
    it { is_expected.to belong_to(:oni_sorceress_requestable).optional }
  end
end
