# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_bogofilter_hams
#
#  id                                     :uuid             not null, primary key
#  learn_state                            :integer          default("not_learned"), not null
#  oni_sorceress_bogofilter_hammable_type :string           not null
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_bogofilter_hammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_hams_on_hammable  (oni_sorceress_bogofilter_hammable_type,oni_sorceress_bogofilter_hammable_id)
#
RSpec.describe OniSorceress::Analytics::BogofilterHam do
  subject { build(:oni_sorceress_analytics_bogofilter_ham) }

  describe "associations" do
    it { is_expected.to belong_to(:oni_sorceress_bogofilter_hammable) }
  end

  it { is_expected.to define_enum_for(:learn_state).with_values([:not_learned, :learned, :unlearned]) }
end
