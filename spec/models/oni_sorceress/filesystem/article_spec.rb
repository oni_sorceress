# frozen_string_literal: true

require "fileutils"
require_relative "../../../../app/models/oni_sorceress/filesystem/article"

describe OniSorceress::Filesystem::Article do
  it "provides access to article headers and body" do
    article = described_class.new(
      file: {
        headers: {
          foo: "bar",
        },
        body: "test body",
      },
    )

    expect(article.headers[:foo]).to eq("bar")
    expect(article.body).to eq("test body")
  end

  it "provides access to attachments" do
    repo = File.join(__dir__, "../fixtures/fsrepotest")
    FileUtils.mkdir_p(File.join(repo, "articles", "foo", "attachments"))
    File.write(File.join(repo, "articles", "foo", "foo.md"), "test file")
    File.write(File.join(repo, "articles", "foo", "attachments", "porn.zip"), "test archive")

    article = described_class.new(
      file: {
        headers: {
          base_dir: File.join(repo, "articles", "foo"),
        },
      },
    )

    expect(article.attachments.length).to eq(1)
    expect(article.attachments[0]).to be_an_instance_of(OniSorceress::Filesystem::Attachment)
    expect(article.attachments[0].headers[:path]).to include("porn.zip")

    FileUtils.rm_rf(repo)
  end

  it "provides access to images" do
    repo = File.join(__dir__, "../fixtures/fsrepotest")
    FileUtils.mkdir_p(File.join(repo, "articles", "foo", "images"))
    File.write(File.join(repo, "articles", "foo", "foo.md"), "test file")
    File.write(File.join(repo, "articles", "foo", "images", "1.jpg"), "test image")

    article = described_class.new(
      file: {
        headers: {
          base_dir: File.join(repo, "articles", "foo"),
        },
      },
    )

    expect(article.images.length).to eq(1)
    expect(article.images[0]).to be_an_instance_of(OniSorceress::Filesystem::Image)
    expect(article.images[0].headers[:path]).to include("1.jpg")

    FileUtils.rm_rf(repo)
  end
end
