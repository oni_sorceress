# frozen_string_literal: true

require_relative "../../../../app/models/oni_sorceress/filesystem/image"

describe OniSorceress::Filesystem::Image do
  it "provides access to attachment headers and body" do
    image = described_class.new(
      file: {
        headers: {
          foo: "bar",
        },
        body: "test body",
      },
    )

    expect(image.headers[:foo]).to eq("bar")
    expect(image.body).to eq("test body")
  end
end
