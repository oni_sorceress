# frozen_string_literal: true

require_relative "../../../../app/models/oni_sorceress/filesystem/attachment"

describe OniSorceress::Filesystem::Attachment do
  it "provides access to attachment headers and body" do
    attachment = described_class.new(
      file: {
        headers: {
          foo: "bar",
        },
        body: "test body",
      },
    )

    expect(attachment.headers[:foo]).to eq("bar")
    expect(attachment.body).to eq("test body")
  end
end
