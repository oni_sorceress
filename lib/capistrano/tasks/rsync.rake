# frozen_string_literal: true

namespace :deploy do
  desc "+ Upload application via Rsync"
  task :upload_app do |_t|
    FileUtils.rm_rf("tmp/_rsync_cache")
    FileUtils.mkdir("tmp/_rsync_cache")

    user = fetch(:deploy_user)
    host = fetch(:deploy_host)

    if !user || !host
      puts "User or host is missing in the configuration"
      exit(1)
    end

    %x(git clone --no-hardlinks --local . tmp/_rsync_cache)
    system("rsync --rsh='ssh' tmp/_rsync_cache/ #{user}@#{host}:_uploaded --verbose --recursive --delete --compress --progress")
  end

  desc "Generate backgrounds"
  task :backgrounds do |_t|
    on primary(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "generate_backgrounds"
        end
      end
    end
  end
end

before "deploy:starting", "deploy:upload_app"
after "deploy:publishing", "deploy:backgrounds"
