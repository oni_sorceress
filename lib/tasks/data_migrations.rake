# frozen_string_literal: true

namespace :data_migrations do
  desc "Migrate comment#published_at"
  task published_at: :environment do
    OniSorceress::Content::Comment.where(published: true).find_each do |comment|
      comment.update(published_at: comment.updated_at)
    end
  end
end
