# frozen_string_literal: true

require "fileutils"

desc "generate backgrounds versions and copy to public"
task generate_backgrounds: :environment do |_t|
  portrait_dir = Rails.public_path.join("media/backgrounds/portrait")
  landscape_dir = Rails.public_path.join("media/backgrounds/landscape")
  FileUtils.mkdir_p(portrait_dir)
  FileUtils.mkdir_p(landscape_dir)
  bg_config = [
    { name: "480", size: "640x480" },
    { name: "720", size: "1280x720" },
    { name: "1080", size: "1920x1080" },
    { name: "4k", size: "4096x2160" },
  ]

  bgs = OniSorceress::Filesystem::BackgroundRepository.all.reduce([]) do |processed_backgrounds, background|
    bg_config.each do |resolution|
      image = MiniMagick::Image.open(background[:headers][:path])

      filename = File.basename(background[:headers][:path], ".*")
      resized_filename = if image.width < image.height
        portrait_dir.join("#{filename}_#{resolution[:name]}.jpg")
      else
        landscape_dir.join("#{filename}_#{resolution[:name]}.jpg")
      end

      if File.exist?(resized_filename)
        printf("_")
        processed_backgrounds << resized_filename.to_s
        next
      end

      image.resize(resolution[:size])
      image.format("jpg")
      image.quality("60")
      image.write(resized_filename)

      printf(".")
      processed_backgrounds << resized_filename.to_s
    end
    processed_backgrounds
  end

  excessive_portrait = Dir["#{portrait_dir}/*"] - bgs
  excessive_landscape = Dir["#{landscape_dir}/*"] - bgs

  if !excessive_portrait.empty?
    FileUtils.rm(excessive_portrait)
    puts "*"
  end

  if !excessive_landscape.empty?
    FileUtils.rm(excessive_landscape)
    puts "*"
  end
  puts
end
