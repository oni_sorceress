# frozen_string_literal: true

namespace :fs do
  desc "list articles in filesystem"
  task list: :environment do |_t|
    articles = OniSorceress::Filesystem::ArticleRepository
      .all
      .map { |article| OniSorceress::Filesystem::ArticleDecorator.call(article) }
      .sort_by do |a|
        a[:world]
        a[:section]
        a[:last_posted] || "_not posted_"
        a[:author] || "_no author_"
        + a[:uri]
      end
    articles.each do |a|
      puts "[#{a[:world]}/#{a[:section]}] \
'#{a[:last_posted] || "_not posted_"}' '#{a[:author] || "_no author_"}' \
'#{a[:poster]}' '#{a[:uri]}'"
    end
    puts "\nTotal: #{articles.size}\n"
  end
end
