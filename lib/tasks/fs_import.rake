# frozen_string_literal: true

namespace :fs do
  desc "Import/update articles to the database"
  task import_articles: :environment do
    OniSorceress::FilesystemImporter::ModelImporters::Article.call(root_path: ENV.fetch("CONTENT_PATH", nil))
  end
end
