# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2024_10_27_224052) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"
  enable_extension "pgcrypto"

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "oni_sorceress_analytics_bogofilter_hams", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "learn_state", default: 0, null: false
    t.string "oni_sorceress_bogofilter_hammable_type", null: false
    t.uuid "oni_sorceress_bogofilter_hammable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_bogofilter_hammable_type", "oni_sorceress_bogofilter_hammable_id"], name: "index_oni_sorceress_analytics_hams_on_hammable"
  end

  create_table "oni_sorceress_analytics_bogofilter_spams", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "learn_state", default: 0, null: false
    t.string "oni_sorceress_bogofilter_spammable_type", null: false
    t.uuid "oni_sorceress_bogofilter_spammable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_bogofilter_spammable_type", "oni_sorceress_bogofilter_spammable_id"], name: "index_oni_sorceress_analytics_spams_on_spammable"
  end

  create_table "oni_sorceress_analytics_requests", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "uid"
    t.jsonb "data"
    t.integer "status_code"
    t.datetime "application_server_request_start"
    t.datetime "application_server_request_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "oni_sorceress_requestable_type"
    t.uuid "oni_sorceress_requestable_id"
    t.index ["oni_sorceress_requestable_type", "oni_sorceress_requestable_id"], name: "index_oni_sorceress_analytics_requests_on_requestable"
  end

  create_table "oni_sorceress_analytics_spam_checks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.float "score", null: false
    t.integer "classifier", null: false
    t.integer "result", null: false
    t.string "oni_sorceress_spammable_type", null: false
    t.uuid "oni_sorceress_spammable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_spammable_type", "oni_sorceress_spammable_id"], name: "index_oni_sorceress_analytics_spam_checks_on_spammable"
  end

  create_table "oni_sorceress_content_articles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "pretty_id", null: false
    t.text "title"
    t.text "title_mime"
    t.text "body"
    t.text "body_mime"
    t.text "original_filename"
    t.text "uri"
    t.boolean "hidden", default: false, null: false
    t.text "secret"
    t.datetime "posted_at"
    t.datetime "last_updated_at"
    t.uuid "oni_sorceress_content_author_id"
    t.uuid "oni_sorceress_content_category_id"
    t.uuid "oni_sorceress_content_license_id"
    t.uuid "oni_sorceress_content_world_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_author_id"], name: "index_oni_sorceress_content_articles_on_author_id"
    t.index ["oni_sorceress_content_category_id"], name: "index_oni_sorceress_content_articles_on_category_id"
    t.index ["oni_sorceress_content_license_id"], name: "index_oni_sorceress_content_articles_on_license_id"
    t.index ["oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_articles_on_world_id"
    t.index ["pretty_id"], name: "index_oni_sorceress_content_articles_on_pretty_id", unique: true
  end

  create_table "oni_sorceress_content_articles_languages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "oni_sorceress_content_article_id", null: false
    t.uuid "oni_sorceress_content_language_id", null: false
    t.integer "priority", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_article_id", "oni_sorceress_content_language_id"], name: "idx_on_oni_sorceress_content_article_id_oni_sorcere_327f950961", unique: true
    t.index ["oni_sorceress_content_article_id"], name: "index_oni_sorceress_content_art_lang_on_article_id"
    t.index ["oni_sorceress_content_language_id"], name: "index_oni_sorceress_content_art_lang_on_language_id"
  end

  create_table "oni_sorceress_content_authors", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "bio"
    t.text "name", null: false
    t.uuid "oni_sorceress_content_world_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_world_id", "name"], name: "idx_on_oni_sorceress_content_world_id_name_c3b865a681", unique: true
    t.index ["oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_authors_on_world_id"
  end

  create_table "oni_sorceress_content_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "first_level", null: false
    t.text "second_level", null: false
    t.uuid "oni_sorceress_content_world_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["first_level", "second_level", "oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_categories_on_first_second_world", unique: true
    t.index ["oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_categories_on_world_id"
  end

  create_table "oni_sorceress_content_comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "body", null: false
    t.text "poster_name"
    t.string "oni_sorceress_content_commentable_type", null: false
    t.uuid "oni_sorceress_content_commentable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "published_at"
    t.index ["oni_sorceress_content_commentable_type", "oni_sorceress_content_commentable_id"], name: "index_oni_sorceress_content_comment_on_commentable"
  end

  create_table "oni_sorceress_content_image_galleries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "name"
    t.integer "position", default: 9999
    t.string "oni_sorceress_content_image_galleryable_type", null: false
    t.uuid "oni_sorceress_content_image_galleryable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_image_galleryable_type", "oni_sorceress_content_image_galleryable_id"], name: "index_oni_sorceress_content_image_on_image_galleryable"
  end

  create_table "oni_sorceress_content_images", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "oni_sorceress_content_imageable_type", null: false
    t.uuid "oni_sorceress_content_imageable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_imageable_type", "oni_sorceress_content_imageable_id"], name: "index_oni_sorceress_content_image_on_imageable"
  end

  create_table "oni_sorceress_content_languages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "code", null: false
    t.uuid "oni_sorceress_content_world_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_world_id", "code"], name: "idx_on_oni_sorceress_content_world_id_code_2114b11a50", unique: true
    t.index ["oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_languages_on_world_id"
  end

  create_table "oni_sorceress_content_licenses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "abbr", null: false
    t.text "link"
    t.text "name", null: false
    t.uuid "oni_sorceress_content_world_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_oni_sorceress_content_licenses_on_name", unique: true
    t.index ["oni_sorceress_content_world_id", "abbr"], name: "idx_on_oni_sorceress_content_world_id_abbr_66a0375cc7", unique: true
    t.index ["oni_sorceress_content_world_id"], name: "index_oni_sorceress_content_licenses_on_world_id"
  end

  create_table "oni_sorceress_content_videos", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "oni_sorceress_content_videoable_type", null: false
    t.uuid "oni_sorceress_content_videoable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oni_sorceress_content_videoable_type", "oni_sorceress_content_videoable_id"], name: "index_oni_sorceress_content_image_on_videoable"
  end

  create_table "oni_sorceress_content_worlds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_oni_sorceress_content_worlds_on_name", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "oni_sorceress_content_articles", "oni_sorceress_content_authors"
  add_foreign_key "oni_sorceress_content_articles", "oni_sorceress_content_categories"
  add_foreign_key "oni_sorceress_content_articles", "oni_sorceress_content_licenses"
  add_foreign_key "oni_sorceress_content_articles", "oni_sorceress_content_worlds"
  add_foreign_key "oni_sorceress_content_articles_languages", "oni_sorceress_content_articles"
  add_foreign_key "oni_sorceress_content_articles_languages", "oni_sorceress_content_languages"
  add_foreign_key "oni_sorceress_content_authors", "oni_sorceress_content_worlds"
  add_foreign_key "oni_sorceress_content_categories", "oni_sorceress_content_worlds"
  add_foreign_key "oni_sorceress_content_languages", "oni_sorceress_content_worlds"
  add_foreign_key "oni_sorceress_content_licenses", "oni_sorceress_content_worlds"
end
