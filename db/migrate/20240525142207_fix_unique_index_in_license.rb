# frozen_string_literal: true

class FixUniqueIndexInLicense < ActiveRecord::Migration[7.1]
  def change
    remove_index(:oni_sorceress_content_licenses, :abbr, name: :index_oni_sorceress_content_licenses_on_abbr)
    add_index(:oni_sorceress_content_licenses, [:oni_sorceress_content_world_id, :abbr], unique: true)
  end
end
