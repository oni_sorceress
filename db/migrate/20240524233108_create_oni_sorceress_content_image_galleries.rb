# frozen_string_literal: true

class CreateOniSorceressContentImageGalleries < ActiveRecord::Migration[7.1]
  def change
    create_table(:oni_sorceress_content_image_galleries, id: :uuid) do |t|
      t.text(:name)
      t.integer(:position, default: 9999)
      t.references(
        :oni_sorceress_content_image_galleryable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_content_image_on_image_galleryable" },
      )

      t.timestamps
    end
  end
end
