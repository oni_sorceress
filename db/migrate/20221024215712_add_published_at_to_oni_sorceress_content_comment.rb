# frozen_string_literal: true

class AddPublishedAtToOniSorceressContentComment < ActiveRecord::Migration[7.0]
  def change
    add_column(:oni_sorceress_content_comments, :published_at, :datetime)
  end
end
