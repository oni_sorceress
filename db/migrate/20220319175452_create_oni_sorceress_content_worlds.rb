# frozen_string_literal: true

class CreateOniSorceressContentWorlds < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_worlds, id: :uuid) do |t|
      t.text(:name, null: false)
      t.index(:name, unique: true)

      t.timestamps
    end
  end
end
