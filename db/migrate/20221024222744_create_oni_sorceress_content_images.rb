# frozen_string_literal: true

class CreateOniSorceressContentImages < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_images, id: :uuid) do |t|
      t.references(
        :oni_sorceress_content_imageable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_content_image_on_imageable" },
      )

      t.timestamps
    end
  end
end
