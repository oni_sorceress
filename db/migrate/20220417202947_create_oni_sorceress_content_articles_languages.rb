# frozen_string_literal: true

class CreateOniSorceressContentArticlesLanguages < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_articles_languages, id: :uuid) do |t|
      t.references(
        :oni_sorceress_content_article,
        null: false,
        foreign_key: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_content_art_lang_on_article_id" },
      )
      t.references(
        :oni_sorceress_content_language,
        null: false,
        foreign_key: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_content_art_lang_on_language_id" },
      )
      t.integer(:priority, null: false, default: 1)

      t.timestamps
    end
  end
end
