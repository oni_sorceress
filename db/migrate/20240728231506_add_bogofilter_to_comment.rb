# frozen_string_literal: true

class AddBogofilterToComment < ActiveRecord::Migration[7.1]
  def change
    remove_column(:oni_sorceress_content_comments, :published, :boolean, null: false, default: false)

    create_table(:oni_sorceress_analytics_bogofilter_hams, id: :uuid) do |t|
      t.integer(:learn_state, null: false, default: 0)
      t.references(
        :oni_sorceress_bogofilter_hammable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_analytics_hams_on_hammable" },
      )
      t.timestamps
    end

    create_table(:oni_sorceress_analytics_bogofilter_spams, id: :uuid) do |t|
      t.integer(:learn_state, null: false, default: 0)
      t.references(
        :oni_sorceress_bogofilter_spammable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_analytics_spams_on_spammable" },
      )
      t.timestamps
    end
  end
end
