# frozen_string_literal: true

class FixUniqueIndexInLanguages < ActiveRecord::Migration[7.1]
  def change
    remove_index(:oni_sorceress_content_languages, :code, name: :index_oni_sorceress_content_languages_on_code)
    add_index(:oni_sorceress_content_languages, [:oni_sorceress_content_world_id, :code], unique: true)
  end
end
