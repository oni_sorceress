# frozen_string_literal: true

class CreateOniSorceressContentVideos < ActiveRecord::Migration[7.1]
  def change
    create_table(:oni_sorceress_content_videos, id: :uuid) do |t|
      t.references(
        :oni_sorceress_content_videoable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_content_image_on_videoable" },
      )
      t.timestamps
    end
  end
end
