# frozen_string_literal: true

class CreateOniSorceressContentComments < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_comments, id: :uuid) do |t|
      t.text(:body, null: false)
      t.text(:poster_name)
      t.boolean(:published, default: false, null: false)
      t.references(
        :oni_sorceress_content_commentable,
        null: false,
        type: :uuid,
        polymorphic: true,
        index: { name: "index_oni_sorceress_content_comment_on_commentable" },
      )

      t.timestamps
    end
  end
end
