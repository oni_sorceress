# frozen_string_literal: true

class CreateOniSorceressContentCategories < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_categories, id: :uuid) do |t|
      t.text(:first_level, null: false)
      t.text(:second_level, null: false)
      t.references(
        :oni_sorceress_content_world,
        null: false,
        foreign_key: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_content_categories_on_world_id" },
      )
      t.index([:first_level, :second_level], unique: true, name: "index_oni_sorceress_content_categories_on_first_and_second")

      t.timestamps
    end
  end
end
