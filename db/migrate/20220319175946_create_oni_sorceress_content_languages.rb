# frozen_string_literal: true

class CreateOniSorceressContentLanguages < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_languages, id: :uuid) do |t|
      t.text(:code, null: false)
      t.index(:code, unique: true)
      t.references(
        :oni_sorceress_content_world,
        null: false,
        foreign_key: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_content_languages_on_world_id" },
      )

      t.timestamps
    end
  end
end
