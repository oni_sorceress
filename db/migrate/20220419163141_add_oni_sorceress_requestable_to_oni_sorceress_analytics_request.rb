# frozen_string_literal: true

class AddOniSorceressRequestableToOniSorceressAnalyticsRequest < ActiveRecord::Migration[7.0]
  def change
    add_reference(
      :oni_sorceress_analytics_requests,
      :oni_sorceress_requestable,
      null: true,
      polymorphic: true,
      type: :uuid,
      index: { name: "index_oni_sorceress_analytics_requests_on_requestable" },
    )
  end
end
