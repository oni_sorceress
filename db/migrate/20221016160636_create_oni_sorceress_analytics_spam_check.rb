# frozen_string_literal: true

class CreateOniSorceressAnalyticsSpamCheck < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_analytics_spam_checks, id: :uuid) do |t|
      t.float(:score, null: false)
      t.integer(:classifier, null: false)
      t.integer(:result, null: false)
      t.references(
        :oni_sorceress_spammable,
        null: false,
        polymorphic: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_analytics_spam_checks_on_spammable" },
      )

      t.timestamps
    end
  end
end
