# frozen_string_literal: true

class CreateOniSorceressAnalyticsRequests < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_analytics_requests, id: :uuid) do |t|
      t.text(:uid)
      t.jsonb(:data)
      t.integer(:status_code)
      t.datetime(:application_server_request_start)
      t.datetime(:application_server_request_end)

      t.timestamps
    end
  end
end
