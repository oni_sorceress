# frozen_string_literal: true

class CreateOniSorceressContentAuthors < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_authors, id: :uuid) do |t|
      t.text(:bio)
      t.text(:name, null: false)
      t.index(:name, unique: true)
      t.references(
        :oni_sorceress_content_world,
        null: false,
        foreign_key: true,
        type: :uuid,
        index: { name: "index_oni_sorceress_content_authors_on_world_id" },
      )

      t.timestamps
    end
  end
end
