# frozen_string_literal: true

class AddUniqueIndexToArticlesLanguages < ActiveRecord::Migration[7.1]
  def change
    add_index(:oni_sorceress_content_articles_languages, [:oni_sorceress_content_article_id, :oni_sorceress_content_language_id], unique: true)
  end
end
