# frozen_string_literal: true

class CreateOniSorceressContentArticles < ActiveRecord::Migration[7.0]
  def change
    create_table(:oni_sorceress_content_articles, id: :uuid) do |t|
      t.text(:pretty_id, null: false)
      t.index(:pretty_id, unique: true)
      t.text(:title)
      t.text(:title_mime)
      t.text(:body)
      t.text(:body_mime)
      t.text(:original_filename)
      t.text(:uri)
      t.boolean(:hidden, default: false, null: false)
      t.text(:secret)
      t.datetime(:posted_at)
      t.datetime(:last_updated_at)
      t.references(:oni_sorceress_content_author, type: :uuid, foreign_key: true, index: { name: "index_oni_sorceress_content_articles_on_author_id" })
      t.references(:oni_sorceress_content_category, type: :uuid, foreign_key: true, index: { name: "index_oni_sorceress_content_articles_on_category_id" })
      t.references(:oni_sorceress_content_license, type: :uuid, foreign_key: true, index: { name: "index_oni_sorceress_content_articles_on_license_id" })
      t.references(:oni_sorceress_content_world, type: :uuid, foreign_key: true, index: { name: "index_oni_sorceress_content_articles_on_world_id" })

      t.timestamps
    end
  end
end
