# frozen_string_literal: true

class CategoryUniquenessWithWorld < ActiveRecord::Migration[7.2]
  def change
    remove_index(
      :oni_sorceress_content_categories,
      [:first_level, :second_level],
      unique: true,
      name: "index_oni_sorceress_content_categories_on_first_and_second",
    )
    add_index(
      :oni_sorceress_content_categories,
      [:first_level, :second_level, :oni_sorceress_content_world_id],
      unique: true,
      name: "index_oni_sorceress_content_categories_on_first_second_world",
    )
  end
end
