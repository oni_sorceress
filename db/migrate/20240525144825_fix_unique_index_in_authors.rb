# frozen_string_literal: true

class FixUniqueIndexInAuthors < ActiveRecord::Migration[7.1]
  def change
    remove_index(:oni_sorceress_content_authors, :name, name: :index_oni_sorceress_content_authors_on_name)
    add_index(:oni_sorceress_content_authors, [:oni_sorceress_content_world_id, :name], unique: true)
  end
end
