# Oni Sorceress

Oni Sorceress is a content management system focused on writing content as files instead of web interface and then uploading it with a command.

Key features:

- write articles in Markdown files
- add images and galleries to articles
- a support for comments
- ATOM feed
- uses Ruby on Rails

There is more planned.

## Philosophy

I created ONI Sorceress for self expression for creative people (me). To have own place on the Internet and be technologically independent from content hosting services and support a wide variety of content.

## License

Copyright 2014-2024 Fierce Eyes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
