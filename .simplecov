# frozen_string_literal: true

SimpleCov.start("rails") do
  command_name "ONI SORCERESS"
  minimum_coverage 90

  enable_coverage :branch
  primary_coverage :branch
  enable_coverage_for_eval

  add_group "Components", "app/components"
  add_group "Decorators", "app/decorators"
  add_group "Forms", "app/forms"
  add_group "Lib", "app/lib"
  add_group "Repositories", "app/repositories"

  add_filter [
    "bin",
    "coverage",
    "db",
    "log",
    "node_modules",
    "public",
    "spec",
    "storage",
    "test",
    "tmp",
    "vendor",
  ]
end
