# frozen_string_literal: true

require Rails.root.join("app/models/application_record").to_s
require Rails.root.join("app/models/oni_sorceress_record").to_s
require Rails.root.join("app/models/oni_sorceress/analytics").to_s
require Rails.root.join("app/models/oni_sorceress/analytics/request").to_s

Rails.configuration.before_initialize do
  Rails.application.config.middleware.use(
    RackRequestObjectLogger,
    OniSorceress::Analytics::Request,
  )
end
