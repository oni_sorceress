# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/* (remember to link manifest in application.html.erb)
  # get "manifest" => "rails/pwa#manifest", as: :pwa_manifest
  # get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker

  # Defines the root path route ("/")
  # root "posts#index"
  scope module: "oni_sorceress" do
    get "allfeed.atom" => "outer_comm/atom#index", as: :allfeed_atom, format: false
    get "robots.txt" => "outer_comm/robots_txt#show", as: :robots_txt, format: false
    get "sitemap.xml" => "outer_comm/sitemap_xml#show", as: :sitemap_xml, format: false

    scope module: "front" do
      get "browse", to: "browse_by_category#index"
      get "categories/:first_level", to: "first_level_category#show", as: "first_level_category"
      get "categories/:first_level/:second_level", to: "second_level_category#show", as: "second_level_category"

      resources :articles, only: [:index, :show] do
        resources :comments, only: :create
      end
      resources :authors, only: :show

      root to: "beginnings#index"
    end

    scope module: "web_services" do
      resource :what_is_my_ip_address, only: :show
    end

    namespace "administration" do
      resources :style_guide, only: :index, if: -> { Rails.env.development? } do
        collection do
          get :articles
          get :articles_mobile
        end
      end

      namespace :moderation do
        resources :comments, only: [:index]
      end
    end
  end
end
