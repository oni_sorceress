# frozen_string_literal: true

module OniSorceress
  def self.table_name_prefix
    "oni_sorceress_"
  end
end
