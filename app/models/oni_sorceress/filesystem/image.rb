# frozen_string_literal: true

class OniSorceress::Filesystem::Image
  def initialize(file:)
    @file = file
  end

  def body
    @file[:body]
  end

  def headers
    @file[:headers]
  end
end
