# frozen_string_literal: true

class OniSorceress::Filesystem::Article
  def initialize(file:)
    @file = file
  end

  def body
    @file[:body]
  end

  def headers
    @file[:headers]
  end

  def attachments
    OniSorceress::Filesystem::AttachmentRepository.all_attachments(@file)
  end

  def images
    OniSorceress::Filesystem::AttachmentRepository.all_images(@file)
  end

  def image_galleries
    OniSorceress::Filesystem::AttachmentRepository.all_image_galleries(@file)
  end
end
