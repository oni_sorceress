# frozen_string_literal: true

class OniSorceress::Filesystem::Attachment
  def initialize(file:)
    @file = file
  end

  def body
    @file[:body]
  end

  def headers
    @file[:headers]
  end
end
