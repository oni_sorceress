# frozen_string_literal: true

module ::OniSorceress::Analytics
  def self.table_name_prefix
    "oni_sorceress_analytics_"
  end
end
