# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_image_galleries
#
#  id                                           :uuid             not null, primary key
#  name                                         :text
#  oni_sorceress_content_image_galleryable_type :string           not null
#  position                                     :integer          default(9999)
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  oni_sorceress_content_image_galleryable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_image_galleryable  (oni_sorceress_content_image_galleryable_type,oni_sorceress_content_image_galleryable_id)
#
class OniSorceress::Content::ImageGallery < ApplicationRecord
  belongs_to :oni_sorceress_content_image_galleryable, polymorphic: true

  has_many :images, as: :oni_sorceress_content_imageable, dependent: :destroy
end
