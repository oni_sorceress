# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_images
#
#  id                                   :uuid             not null, primary key
#  oni_sorceress_content_imageable_type :string           not null
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  oni_sorceress_content_imageable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_imageable  (oni_sorceress_content_imageable_type,oni_sorceress_content_imageable_id)
#
class OniSorceress::Content::Image < ApplicationRecord
  belongs_to :oni_sorceress_content_imageable, polymorphic: true

  # TODO: jpegsave
  has_one_attached :file do |attachable|
    attachable.variant(
      :jpeg_vga,
      resize_to_limit: [600, nil],
      format: :jpeg,
      saver: { subsample_mode: "on", strip: true, interlace: false, quality: 80, jpegsave: "aaa.jpg" },
      preprocessed: true,
    )
    attachable.variant(
      :jpeg_hd,
      resize_to_limit: [1200, nil],
      format: :jpeg,
      saver: { subsample_mode: "on", strip: true, interlace: false, quality: 80, jpegsave: "aaa.jpg" },
      preprocessed: true,
    )
    attachable.variant(
      :jpeg_qhd,
      resize_to_limit: [2200, nil],
      format: :jpeg,
      saver: { subsample_mode: "on", strip: true, interlace: false, quality: 80, jpegsave: "aaa.jpg" },
      preprocessed: true,
    )
    attachable.variant(
      :jpeg_uhd,
      resize_to_limit: [3200, nil],
      format: :jpeg,
      saver: { subsample_mode: "on", strip: true, interlace: false, quality: 80, jpegsave: "aaa.jpg" },
      preprocessed: true,
    )

    # attachable.variant :avif_vga, resize_to_limit: [600, nil], format: :heif,
    #  saver: { subsample_mode: "on", quality: 80, compression: "av1" }, preprocessed: true
    # attachable.variant :avif_hd, resize_to_limit: [1200, nil], format: :heif,
    #  saver: { subsample_mode: "on", quality: 80, compression: "av1" }, preprocessed: true
    # attachable.variant :avif_qhd, resize_to_limit: [2200, nil], format: :heif,
    #  saver: { subsample_mode: "on", quality: 80, compression: "av1" }, preprocessed: true
    # attachable.variant :avif_uhd, resize_to_limit: [3200, nil], format: :heif,
    #  saver: { subsample_mode: "on", quality: 80, compression: "av1" }, preprocessed: true
  end

  validates :file, presence: true
end
