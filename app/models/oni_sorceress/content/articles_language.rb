# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_articles_languages
#
#  id                                :uuid             not null, primary key
#  priority                          :integer          default(1), not null
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  oni_sorceress_content_article_id  :uuid             not null
#  oni_sorceress_content_language_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_article_id_oni_sorcere_327f950961  (oni_sorceress_content_article_id,oni_sorceress_content_language_id) UNIQUE
#  index_oni_sorceress_content_art_lang_on_article_id              (oni_sorceress_content_article_id)
#  index_oni_sorceress_content_art_lang_on_language_id             (oni_sorceress_content_language_id)
#
# Foreign Keys
#
#  fk_rails_59879288e1  (oni_sorceress_content_article_id => oni_sorceress_content_articles.id)
#  fk_rails_a8fb0dab09  (oni_sorceress_content_language_id => oni_sorceress_content_languages.id)
#
class OniSorceress::Content::ArticlesLanguage < OniSorceressRecord
  belongs_to :article, class_name: "::OniSorceress::Content::Article", foreign_key: "oni_sorceress_content_article_id", inverse_of: :articles_languages
  belongs_to :language, class_name: "::OniSorceress::Content::Language", foreign_key: "oni_sorceress_content_language_id", inverse_of: false

  validates :oni_sorceress_content_language_id, uniqueness: { scope: :oni_sorceress_content_article_id }
end
