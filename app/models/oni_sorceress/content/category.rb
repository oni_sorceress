# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_categories
#
#  id                             :uuid             not null, primary key
#  first_level                    :text             not null
#  second_level                   :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_categories_on_first_second_world  (first_level,second_level,oni_sorceress_content_world_id) UNIQUE
#  index_oni_sorceress_content_categories_on_world_id            (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_90d1cbdda4  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
class OniSorceress::Content::Category < OniSorceressRecord
  belongs_to :world, class_name: "::OniSorceress::Content::World", foreign_key: "oni_sorceress_content_world_id", inverse_of: :categories
  has_many :articles,
    class_name: "::OniSorceress::Content::Article",
    foreign_key: "oni_sorceress_content_category_id",
    dependent: :destroy,
    inverse_of: :category

  validates :first_level, presence: true
  validates :second_level, presence: true, uniqueness: { scope: [:first_level, :oni_sorceress_content_world_id] }

  scope :in_world, ->(world) do
    includes(:world)
      .joins(:articles)
      .where(world: { name: world })
      .where(articles: { hidden: false })
      .order(:first_level, :second_level)
  end

  scope :in_world_first_level, ->(world) do
    includes(:world)
      .joins(:articles)
      .where(world: { name: world })
      .where(articles: { hidden: false })
      .distinct
      .order(:first_level)
      .pluck(:first_level)
  end
end
