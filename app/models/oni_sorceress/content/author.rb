# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_authors
#
#  id                             :uuid             not null, primary key
#  bio                            :text
#  name                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_name_c3b865a681  (oni_sorceress_content_world_id,name) UNIQUE
#  index_oni_sorceress_content_authors_on_world_id        (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_e47066d1b2  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
class OniSorceress::Content::Author < OniSorceressRecord
  belongs_to :world, class_name: "::OniSorceress::Content::World", foreign_key: "oni_sorceress_content_world_id", inverse_of: :authors
  has_many :articles, class_name: "::OniSorceress::Content::Article", foreign_key: "oni_sorceress_content_author_id", dependent: :destroy, inverse_of: :author

  validates :name, presence: true, uniqueness: { scope: :oni_sorceress_content_world_id }

  scope :for_world, ->(world) { includes(:world).where(world: { name: world }) }
end
