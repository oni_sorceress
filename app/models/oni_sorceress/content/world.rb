# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_worlds
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_oni_sorceress_content_worlds_on_name  (name) UNIQUE
#
class ::OniSorceress::Content::World < OniSorceressRecord
  validates :name, presence: true, uniqueness: true

  has_many :articles, class_name: "::OniSorceress::Content::Article", foreign_key: "oni_sorceress_content_world_id", dependent: :destroy, inverse_of: :world
  has_many :authors, class_name: "::OniSorceress::Content::Author", foreign_key: "oni_sorceress_content_world_id", dependent: :destroy, inverse_of: :world
  has_many :categories, class_name: "::OniSorceress::Content::Category", foreign_key: "oni_sorceress_content_world_id", dependent: :destroy, inverse_of: :world
  has_many :languages, class_name: "::OniSorceress::Content::Language", foreign_key: "oni_sorceress_content_world_id", dependent: :destroy, inverse_of: :world
  has_many :licenses, class_name: "::OniSorceress::Content::License", foreign_key: "oni_sorceress_content_world_id", dependent: :destroy, inverse_of: :world
end
