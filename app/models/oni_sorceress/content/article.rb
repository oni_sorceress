# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_articles
#
#  id                                :uuid             not null, primary key
#  body                              :text
#  body_mime                         :text
#  hidden                            :boolean          default(FALSE), not null
#  last_updated_at                   :datetime
#  original_filename                 :text
#  posted_at                         :datetime
#  secret                            :text
#  title                             :text
#  title_mime                        :text
#  uri                               :text
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  oni_sorceress_content_author_id   :uuid
#  oni_sorceress_content_category_id :uuid
#  oni_sorceress_content_license_id  :uuid
#  oni_sorceress_content_world_id    :uuid
#  pretty_id                         :text             not null
#
# Indexes
#
#  index_oni_sorceress_content_articles_on_author_id    (oni_sorceress_content_author_id)
#  index_oni_sorceress_content_articles_on_category_id  (oni_sorceress_content_category_id)
#  index_oni_sorceress_content_articles_on_license_id   (oni_sorceress_content_license_id)
#  index_oni_sorceress_content_articles_on_pretty_id    (pretty_id) UNIQUE
#  index_oni_sorceress_content_articles_on_world_id     (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_8c982eeadb  (oni_sorceress_content_author_id => oni_sorceress_content_authors.id)
#  fk_rails_a476655b71  (oni_sorceress_content_license_id => oni_sorceress_content_licenses.id)
#  fk_rails_c481d88894  (oni_sorceress_content_category_id => oni_sorceress_content_categories.id)
#  fk_rails_de2f5c8f1e  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
class OniSorceress::Content::Article < OniSorceressRecord
  belongs_to :author, class_name: "::OniSorceress::Content::Author", foreign_key: "oni_sorceress_content_author_id", inverse_of: :articles
  belongs_to :category, class_name: "::OniSorceress::Content::Category", foreign_key: "oni_sorceress_content_category_id", inverse_of: :articles
  has_many :articles_languages,
    class_name: "::OniSorceress::Content::ArticlesLanguage",
    foreign_key: "oni_sorceress_content_article_id",
    dependent: :destroy,
    inverse_of: :article
  has_many :languages, through: :articles_languages
  belongs_to :license, class_name: "::OniSorceress::Content::License", foreign_key: "oni_sorceress_content_license_id", inverse_of: :articles
  belongs_to :world, class_name: "::OniSorceress::Content::World", foreign_key: "oni_sorceress_content_world_id", inverse_of: :articles

  has_many :comments, as: :oni_sorceress_content_commentable, dependent: :destroy
  has_many :images, as: :oni_sorceress_content_imageable, dependent: :destroy
  has_many :image_galleries, as: :oni_sorceress_content_image_galleryable, dependent: :destroy
  has_many :videos, as: :oni_sorceress_content_videoable, dependent: :destroy

  validates :pretty_id, :title, :original_filename, :body, presence: true
  validate :same_world

  scope :listable, ->(world:) do
    includes(:author, :category, :world)
      .where(hidden: false, secret: nil)
      .where(last_updated_at: ...Time.now.utc)
      .where(world: { name: world })
      .order(last_updated_at: :desc, title: :asc)
  end

  scope :listable_for_first_level_category, ->(world:, name:) do
    listable(world:)
      .includes(:category)
      .where(category: { first_level: name })
  end

  private

  def same_world
    errors.add(:author, "must belong to the same world") if author && world != author.world
    errors.add(:category, "must belong to the same world") if category && world != category.world
    errors.add(:language, "all must belong to the same world") if languages.any? { |language| world != language.world }
    errors.add(:license, "must belong to the same world") if license && world != license.world
  end
end
