# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_videos
#
#  id                                   :uuid             not null, primary key
#  oni_sorceress_content_videoable_type :string           not null
#  created_at                           :datetime         not null
#  updated_at                           :datetime         not null
#  oni_sorceress_content_videoable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_image_on_videoable  (oni_sorceress_content_videoable_type,oni_sorceress_content_videoable_id)
#
class OniSorceress::Content::Video < ApplicationRecord
  belongs_to :oni_sorceress_content_videoable, polymorphic: true

  has_one_attached :file

  validates :file, presence: true
end
