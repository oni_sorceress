# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_licenses
#
#  id                             :uuid             not null, primary key
#  abbr                           :text             not null
#  link                           :text
#  name                           :text             not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  oni_sorceress_content_world_id :uuid             not null
#
# Indexes
#
#  idx_on_oni_sorceress_content_world_id_abbr_66a0375cc7  (oni_sorceress_content_world_id,abbr) UNIQUE
#  index_oni_sorceress_content_licenses_on_name           (name) UNIQUE
#  index_oni_sorceress_content_licenses_on_world_id       (oni_sorceress_content_world_id)
#
# Foreign Keys
#
#  fk_rails_28f0dc90ae  (oni_sorceress_content_world_id => oni_sorceress_content_worlds.id)
#
class OniSorceress::Content::License < OniSorceressRecord
  belongs_to :world, class_name: "::OniSorceress::Content::World", foreign_key: "oni_sorceress_content_world_id", inverse_of: :licenses
  has_many :articles, class_name: "::OniSorceress::Content::Article", foreign_key: "oni_sorceress_content_license_id", dependent: :destroy, inverse_of: :license

  validates :abbr, presence: true, uniqueness: { scope: :oni_sorceress_content_world_id }
  validates :name, presence: true, uniqueness: true
end
