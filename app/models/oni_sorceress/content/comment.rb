# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_content_comments
#
#  id                                     :uuid             not null, primary key
#  body                                   :text             not null
#  oni_sorceress_content_commentable_type :string           not null
#  poster_name                            :text
#  published_at                           :datetime
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_content_commentable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_content_comment_on_commentable  (oni_sorceress_content_commentable_type,oni_sorceress_content_commentable_id)
#

class OniSorceress::Content::Comment < OniSorceressRecord
  belongs_to :oni_sorceress_content_commentable, polymorphic: true
  has_one :request, as: :oni_sorceress_requestable, class_name: "::OniSorceress::Analytics::Request", dependent: :destroy
  has_many :spam_checks, as: :oni_sorceress_spammable, class_name: "::OniSorceress::Analytics::SpamCheck", dependent: :destroy
  has_many :bogofilter_hams, as: :oni_sorceress_bogofilter_hammable, class_name: "::OniSorceress::Analytics::BogofilterHam", dependent: :destroy
  has_many :bogofilter_spams, as: :oni_sorceress_bogofilter_spammable, class_name: "::OniSorceress::Analytics::BogofilterSpam", dependent: :destroy

  validates :poster_name, length: { maximum: 100 }
  validates :body, presence: true, length: { maximum: 4000 }
  validates :request, presence: true

  scope :unpublished, -> { where(published_at: nil) }
  scope :without_manual_spam_check, -> {
    left_outer_joins(:spam_checks).where.not(spam_checks: { classifier: :manual }).or(where(spam_checks: { id: nil }))
  }
  scope :not_spam, -> {
    find_by_sql("#{joins(:spam_checks).where.not(spam_checks: { result: :spam }).to_sql} UNION #{where.missing(:spam_checks).to_sql}")
  }

  # def self.not_spam
  #  left_joins(:spam_checks).select do |comment|
  #    comment.spam_checks.empty? || comment.spam_checks.pluck(:result).all? { |item| item.in?(%w[ham unsure]) }
  #  end
  # end
end
