# frozen_string_literal: true

module ::OniSorceress::Content
  def self.table_name_prefix
    "oni_sorceress_content_"
  end
end
