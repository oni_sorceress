# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_bogofilter_spams
#
#  id                                      :uuid             not null, primary key
#  learn_state                             :integer          default("not_learned"), not null
#  oni_sorceress_bogofilter_spammable_type :string           not null
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  oni_sorceress_bogofilter_spammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_spams_on_spammable  (oni_sorceress_bogofilter_spammable_type,oni_sorceress_bogofilter_spammable_id)
#
class OniSorceress::Analytics::BogofilterSpam < OniSorceressRecord
  include AASM

  belongs_to :oni_sorceress_bogofilter_spammable, polymorphic: true

  enum :learn_state, { not_learned: 0, learned: 1, unlearned: 2 }
  aasm :learn_state, enum: true do
    state :not_learned, initial: true
    state :learned
    state :unlearned

    event :learn, from: :not_learned, to: :learned
    event :unlearn, from: :learned, to: :unlearned
  end
end
