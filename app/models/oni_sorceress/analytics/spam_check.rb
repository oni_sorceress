# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_spam_checks
#
#  id                           :uuid             not null, primary key
#  classifier                   :integer          not null
#  oni_sorceress_spammable_type :string           not null
#  result                       :integer          not null
#  score                        :float            not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  oni_sorceress_spammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_spam_checks_on_spammable  (oni_sorceress_spammable_type,oni_sorceress_spammable_id)
#
class OniSorceress::Analytics::SpamCheck < OniSorceressRecord
  belongs_to :oni_sorceress_spammable, polymorphic: true

  enum :classifier, { manual: 0, bogofilter: 1 }
  enum :result, { unsure: 0, spam: 1, ham: 2 }
end
