# frozen_string_literal: true

# == Schema Information
#
# Table name: oni_sorceress_analytics_bogofilter_hams
#
#  id                                     :uuid             not null, primary key
#  learn_state                            :integer          default("not_learned"), not null
#  oni_sorceress_bogofilter_hammable_type :string           not null
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  oni_sorceress_bogofilter_hammable_id   :uuid             not null
#
# Indexes
#
#  index_oni_sorceress_analytics_hams_on_hammable  (oni_sorceress_bogofilter_hammable_type,oni_sorceress_bogofilter_hammable_id)
#
class OniSorceress::Analytics::BogofilterHam < OniSorceressRecord
  include AASM

  belongs_to :oni_sorceress_bogofilter_hammable, polymorphic: true

  enum :learn_state, { not_learned: 0, learned: 1, unlearned: 2 }
  aasm :learn_state, enum: true do
    state :not_learned, initial: true
    state :learned
    state :unlearned

    event :learn, from: :not_learned, to: :learned
    event :unlearn, from: :learned, to: :unlearned
  end
end
