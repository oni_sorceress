# frozen_string_literal: true

class ::OniSorceressRecord < ApplicationRecord
  self.abstract_class = true
  self.implicit_order_column = :created_at

  connects_to database: { writing: :primary, reading: :primary }
end
