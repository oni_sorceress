# frozen_string_literal: true

module OniSorceress::Filesystem::AttachmentRepository
  def self.all_attachments(file)
    raw_models = ::OniSorceress::Filesystem::FileRepository.all(
      dir: File.join(file[:headers][:base_dir]),
      file_formats: ["zip", "tar.gz", "tgz", "tar.bz2"],
      model: "attachments",
      load: false,
    )

    raw_models.map { |model| ::OniSorceress::Filesystem::Attachment.new(file: model) }
  end

  def self.all_images(file)
    raw_models = ::OniSorceress::Filesystem::FileRepository.all(
      dir: File.join(file[:headers][:base_dir]),
      file_formats: ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"],
      model: "images",
      load: false,
    )

    raw_models.map { |model| ::OniSorceress::Filesystem::Image.new(file: model) }
  end
end
