# frozen_string_literal: true

require_relative "file_repository"
require_relative "../../../models/oni_sorceress/filesystem/article"

class OniSorceress::Filesystem::ArticleRepository
  def self.all(load: true)
    raw_models = ::OniSorceress::Filesystem::FileRepository.all(
      dir: ENV.fetch("CONTENT_PATH", nil),
      file_formats: ["md", "text", "txt", "html", "htm"],
      load:,
      model: "articles",
    )

    raw_models.map { |model| Article.new(file: model) }
  end

  def self.find(uri:, load: true)
    raw_model = ::OniSorceress::Filesystem::FileRepository.find(
      dir: ENV.fetch("CONTENT_PATH", nil),
      file_formats: ["md", "text", "txt", "html"],
      load:,
      model: "articles",
      uri:,
    )

    Article.new(file: raw_model)
  end
end
