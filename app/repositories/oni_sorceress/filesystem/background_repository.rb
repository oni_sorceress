# frozen_string_literal: true

class OniSorceress::Filesystem::BackgroundRepository
  def self.all
    ::OniSorceress::Filesystem::FileRepository.all(
      dir: ENV.fetch("CONTENT_PATH", nil),
      file_formats: ["jpg", "jpeg", "png"],
      load: false,
      model: "backgrounds",
    )
  end
end
