# frozen_string_literal: true

module OniSorceress::Filesystem::FileRepository
  def self.all(model:, dir:, file_formats: [], load: true)
    model_sanitized = File.basename(model)
    scope = Pathname.new(dir).join(model_sanitized)
    query = File.join(
      "**",
      !file_formats.empty? ? "*.{#{file_formats.join(",")}}" : "*",
    )
    file_paths = Dir[scope.join(query)].select { |path| File.file?(path) }
    file_paths.map do |path|
      create_model(path:, model: model_sanitized, load:, dir:)
    end
  end

  def self.find(model:, uri:, dir:, file_formats: [], load: true)
    model_sanitized = File.basename(model)
    uri_sanitized = File.basename(uri)
    scope = Pathname.new(dir).join(model_sanitized)
    query = File.join(
      "**",
      !file_formats.empty? ? "#{uri_sanitized}.{#{file_formats.join(",")}}" : "#{uri_sanitized}.*",
    )

    file_paths = Dir[scope.join(query)].select { |path| File.file?(path) }

    return if file_paths.size.zero?

    create_model(path: file_paths.first, model:, load:, dir:)
  end

  # private

  def self.create_model(path:, model:, load:, dir:)
    model_basedir = File.dirname(path)
    {
      headers: {
        base_dir: File.dirname(path),
        content_dir: dir,
        path:,
        uri_id: File.basename(path, File.extname(path)),
        uri: "#{model}:#{File.basename(path, File.extname(path))}",
      },
      body: load ? File.read(path) : nil,
      image_filenames: Dir[File.join(model_basedir, "images/*")],
      image_galleries: Dir[File.join(model_basedir, "image_galleries/*")].map do |gallery_path|
        {
          name: File.basename(gallery_path),
          image_filenames: Dir[File.join(gallery_path, "*")],
        }
      end,
      video_filenames: Dir[File.join(model_basedir, "videos/*")],
    }
  end
  private_class_method :create_model
end
