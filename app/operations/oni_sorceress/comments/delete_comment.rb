# frozen_string_literal: true

class OniSorceress::Comments::DeleteComment < OniSorceress::ApplicationOperation
  step :define_errors
  step :load_comment
  step :delete

  private

  def load_comment(ctx, comment_id:, **)
    ctx[:comment] = OniSorceress::Content::Comment.find(comment_id)

    true
  end

  def delete(ctx, comment:, **)
    if !comment.destroy
      ctx[:errors] << "Cannot delete comment"
      ctx[:errors] += comment.errors.full_messages
      return false
    end

    true
  end
end
