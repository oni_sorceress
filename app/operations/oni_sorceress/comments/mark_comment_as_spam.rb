# frozen_string_literal: true

class OniSorceress::Comments::MarkCommentAsSpam < OniSorceress::ApplicationOperation
  step :define_errors
  step :load_comment
  step :load_bogofilter

  step :mark_as_spam
  step :unlearn_bogofilter_ham
  step :learn_bogofilter_spam

  private

  def load_comment(ctx, comment_id:, **)
    ctx[:comment] = OniSorceress::Content::Comment.find(comment_id)

    true
  end

  def load_bogofilter(ctx, **)
    ctx[:bogofilter] = Bogofilter.new(dbpath: Rails.application.config.bogofilter_database)

    true
  end

  # TODO: transaction
  def mark_as_spam(ctx, comment:, **)
    spam_check = comment.spam_checks.new(classifier: :manual, result: :spam, score: 100)

    if !spam_check.save
      ctx[:errors] += spam_check.errors.full_messages.map { |error| "SPAM check: #{error}" }
      return false
    end

    true
  end

  def unlearn_bogofilter_ham(ctx, bogofilter:, comment:, **)
    learned_bogofilter_hams = comment.bogofilter_hams.where(learn_state: :learned)
    learned_bogofilter_hams.each do |learned_bogofilter_ham|
      bogofilter_result = bogofilter.remove_ham(comment.poster_name.to_s + "\n" + comment.body.to_s)

      if !bogofilter_result
        ctx[:errors] << "Cannot unlearn comment as HAM in Bogofilter"
        return false
      end

      if !learned_bogofilter_ham.update(learn_state: :unlearned)
        ctx[:errors] += comment.errors.full_messages
        return false
      end
    end

    true
  end

  def learn_bogofilter_spam(ctx, bogofilter:, comment:, **)
    return true if comment.bogofilter_spams.exists?(learn_state: :learned)

    bogofilter_result = bogofilter.add_spam(comment.poster_name.to_s + "\n" + comment.body.to_s)

    if !bogofilter_result
      ctx[:errors] << "Cannot add comment as SPAM to Bogofilter"
      return false
    end

    spam_record = comment.bogofilter_spams.new(learn_state: :learned)

    if !spam_record.save
      ctx[:errors] += spam_record.errors.full_messages
      return false
    end

    true
  end
end
