# frozen_string_literal: true

class OniSorceress::Comments::ApproveComment < OniSorceress::ApplicationOperation
  step :define_errors
  step :load_comment
  step :approve

  private

  def load_comment(ctx, comment_id:, **)
    ctx[:comment] = OniSorceress::Content::Comment.find(comment_id)

    true
  end

  def approve(ctx, comment:, **)
    transaction_result = OniSorceress::Content::Comment.transaction do
      comment.lock!

      if !comment.update(published_at: Time.zone.now)
        ctx[:errors] += comment.errors.full_messages.map { |error| "Comment: #{error}" }
        raise ActiveRecord::Rollback
      end

      mark_as_ham_result = OniSorceress::Comments::MarkCommentAsHam.call(comment_id: comment.id)

      if mark_as_ham_result.failure?
        ctx[:errors] += mark_as_ham_result[:errors]
        raise ActiveRecord::Rollback
      end

      true
    end

    transaction_result
  end
end
