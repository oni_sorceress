# frozen_string_literal: true

class OniSorceress::Comments::MarkCommentAsHam < OniSorceress::ApplicationOperation
  step :define_errors
  step :load_comment
  step :load_bogofilter

  step :mark_as_ham
  step :unlearn_bogofilter_spam
  step :learn_bogofilter_ham

  private

  def load_comment(ctx, comment_id:, **)
    ctx[:comment] = OniSorceress::Content::Comment.find(comment_id)

    true
  end

  def load_bogofilter(ctx, **)
    ctx[:bogofilter] = Bogofilter.new(dbpath: Rails.application.config.bogofilter_database)

    true
  end

  # TODO: transaction
  def mark_as_ham(ctx, comment:, **)
    spam_check = comment.spam_checks.new(classifier: :manual, result: :ham, score: 100)

    if !spam_check.save
      ctx[:errors] += spam_check.errors.full_messages.map { |error| "SPAM check: #{error}" }
      return false
    end

    true
  end

  def unlearn_bogofilter_spam(ctx, bogofilter:, comment:, **)
    learned_bogofilter_spams = comment.bogofilter_spams.where(learn_state: :learned)
    learned_bogofilter_spams.each do |learned_bogofilter_spam|
      bogofilter_result = bogofilter.remove_spam(comment.poster_name.to_s + "\n" + comment.body.to_s)

      if !bogofilter_result
        ctx[:errors] << "Cannot unlearn comment as SPAM in Bogofilter"
        return false
      end

      if !learned_bogofilter_spam.update(learn_state: :unlearned)
        ctx[:errors] += comment.errors.full_messages
        return false
      end
    end

    true
  end

  def learn_bogofilter_ham(ctx, bogofilter:, comment:, **)
    return true if comment.bogofilter_hams.exists?(learn_state: :learned)

    bogofilter_result = bogofilter.add_ham(comment.poster_name.to_s + "\n" + comment.body.to_s)

    if !bogofilter_result
      ctx[:errors] << "Cannot add comment as HAM to Bogofilter"
      return false
    end

    ham_record = comment.bogofilter_hams.new(learn_state: :learned)

    if !ham_record.save
      ctx[:errors] += ham_record.errors.full_messages
      return false
    end

    true
  end
end
