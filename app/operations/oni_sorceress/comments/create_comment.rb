# frozen_string_literal: true

class OniSorceress::Comments::CreateComment < OniSorceress::ApplicationOperation
  step :define_errors
  step :check_count
  step :create
  # step :add_request_details
  step :check_spam

  private

  def check_count(ctx, **)
    if ::OniSorceress::Content::Comment.count >= 100_000
      ctx[:errors] << "Can't create more comments. Don't fuck with the system!"
      return false
    end

    true
  end

  def create(ctx, model:, params:, **)
    comment = model.comments.new(params.slice(:poster_name, :body))

    if comment.save
      ctx[:comment] = comment
    else
      ctx[:errors] += comment.errors.full_messages
      return false
    end

    true
  end

  # def add_request_details(ctx, )

  # end

  def run_spamcheck(ctx, comment:, **)
    bogofilter = Bogofilter.new(dbpath: Rails.application.config.bogofilter_database)
    bogofilter_result = bogofilter.classify(comment.poster_name.to_s + "\n" + comment.body.to_s)
    spam_check = comment.spam_checks.new(
      classifier: :bogofilter,
      **bogofilter_result.slice(:result, :score),
    )

    if spam_check.save
      ctx[:spam_check] = spam_check
    else
      ctx[:errors] += spam_check.errors.full_messages
      return false
    end

    true
  end
end
