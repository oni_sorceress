# frozen_string_literal: true

class OniSorceress::ApplicationOperation < Trailblazer::Operation
  private

  def define_errors(ctx, **)
    ctx[:errors] = []
  end
end
