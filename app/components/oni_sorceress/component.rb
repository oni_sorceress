# frozen_string_literal: true

require "tilt"

module OniSorceress::Component
  def self.render(name, component_variables: {}, **options)
    component = "OniSorceress::Components::#{name.camelcase}::Component".constantize.new(**component_variables)
    template = Tilt.new(Rails.root.join("app", "components", "oni_sorceress", "components", name, "component.html.erb"))
    template.render(component, options)
  end
end
