# frozen_string_literal: true

class OniSorceress::Components::Video::Component
  VideoVersion = Struct.new("VideoVersion", :mimetype, :url)

  attr_reader :height, :preview_url, :title, :variants, :width

  def initialize(
    height:,
    preview_url:,
    title: nil,
    versions:,
    width:
  )
    @height = height ? height.to_i : height
    @preview_url = preview_url
    @title = title
    @variants = versions.reduce([]) do |result, item|
      raise "VideoVersion expected" if !item.is_a?(Struct::VideoVersion)

      result << item
      result
    end
    @width = width ? width.to_i : width
  end
end
