# frozen_string_literal: true

class OniSorceress::Components::Image::Component
  ImageVersion = Struct.new("ImageVersion", :mimetype, :breakpoint, :url)

  attr_reader :breakpoints, :default_breakpoint, :max_breakpoint, :mimetypes, :style, :title, :variants

  def initialize(
    title: nil,
    versions:
  )
    @breakpoints = versions.map { |item| item.breakpoint.to_i }.uniq.sort
    @default_breakpoint = @breakpoints[breakpoints.size / 2]
    @max_breakpoint = @breakpoints.max
    @title = title
    @variants = versions.reduce({}) do |result, item|
      raise "ImageVersion expected" if !item.is_a?(Struct::ImageVersion)

      result[item.mimetype.to_s] ||= {}
      result[item.mimetype.to_s][item.breakpoint.to_i] = item
      result
    end
    @mimetypes = @variants.keys.uniq
    @style = "medium-preview"
  end

  def get_src(mimetype:, breakpoint:)
    variants.dig(mimetype, breakpoint)
  end

  def get_srcset(mimetype:)
    variants.dig(mimetype)&.map do |_breakpoint, image_version|
      "#{image_version.url} #{image_version.breakpoint}w"
    end&.join(", ")&.concat(", #{variants.dig(mimetype, default_breakpoint).url}")
  end
end
