# frozen_string_literal: true

class OniSorceress::Components::Gallery::Component
  attr_reader :images_html, :title

  def initialize(images_html: [], title: nil)
    @images_html = images_html
    @title = title
  end
end
