# frozen_string_literal: true

module OniSorceress::Filesystem::ArticleDecorator
  def self.call(article)
    article_metadata, pure_body = ::OniSorceress::TextFileProcessor.split_metadata_and_content(text: article.body)

    {
      article:,
      file_metadata: article.headers,
      metadata: article_metadata,
      # TODO: images and other stuff
      rendered: pure_body,
      section: ::OniSorceress::TextFileProcessor.second_level_category(
        url: article.headers[:path],
        base_path: article.headers[:content_dir],
      ),
      title: ::OniSorceress::TextFileProcessor.title(text: pure_body),
      uri_id: article.headers[:uri_id],
      uri: article.headers[:uri],
      world: ::OniSorceress::TextFileProcessor.first_level_category(
        url: article.headers[:path],
        base_path: article.headers[:content_dir],
      ),

      first_created: first_value(article_metadata, "created"),
      last_updated: last_value(article_metadata, "updated"),
      last_posted: last_value(article_metadata, "posted"),

      author: string(article_metadata, "author"),
      poster: string(article_metadata, "poster"),

      hidden: bool(article_metadata, "hidden"),
      password: first_value(article_metadata, "password"),
    }
  end

  # private

  def self.first_value(meta, field)
    meta && meta[field]&.min
  end
  private_class_method :first_value

  def self.last_value(meta, field)
    meta && meta[field]&.max
  end
  private_class_method :last_value

  def self.bool(meta, field)
    !!(meta && meta[field])
  end
  private_class_method :bool

  def self.string(meta, field)
    meta && meta[field]&.join(", ")
  end
  private_class_method :string
end
