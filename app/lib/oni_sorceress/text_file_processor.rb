# frozen_string_literal: true

require "pathname"

module ::OniSorceress::TextFileProcessor
  def self.world(url:, base_path:)
    base_dir = File.expand_path(base_path)
    directory = Pathname.new(File.expand_path(File.dirname(url)))
    segments = directory
      .relative_path_from(base_dir)
      .to_s
      .split("/")

    if segments[0] == "articles"
      nil
    else
      segments[0]
    end
  end

  def self.first_level_category(url:, base_path:)
    base_dir = File.expand_path(base_path)
    directory = Pathname.new(File.expand_path(File.dirname(url)))
    segments = directory
      .relative_path_from(base_dir)
      .to_s
      .split("/")

    if segments[0] == "articles"
      segments[1]
    else
      segments[2]
    end
  end

  def self.second_level_category(url:, base_path:)
    base_dir = File.expand_path(base_path)
    directory = Pathname.new(File.expand_path(File.dirname(url)))
    segments = directory
      .relative_path_from(base_dir)
      .to_s
      .split("/")

    if segments[0] == "articles"
      segments[2]
    else
      segments[3]
    end
  end

  def self.title(text:)
    return if !text

    text_no_whitespace = text.lstrip

    return if text_no_whitespace == ""

    title = text_no_whitespace.match(/\A(.+)$/)[1]

    return if !title

    title.gsub("<h1>", "").gsub("</h1>", "").delete_prefix("#").delete_prefix("h1.").strip
  end

  def self.split_metadata_and_content(text:)
    text_no_whitespace = text.lstrip
    metadata_delimiter_index = text_no_whitespace.index("\n\n")

    raw_metadata = metadata_delimiter_index ? text_no_whitespace[0..metadata_delimiter_index] : ""
    content = metadata_delimiter_index ? text_no_whitespace[metadata_delimiter_index..-1] : text_no_whitespace

    [deserialize_metadata(text: raw_metadata), content.lstrip]
  end

  def self.deserialize_metadata(text:)
    text.split("\n").reduce({}) do |result, line|
      key, value = line.split(":", 2).map(&:strip)

      # accept header if not commented out
      if !%r{\A//.*\z}.match?(key)
        result[key] ||= []
        result[key] << (value || "")
      end
      result
    end
  end
end
