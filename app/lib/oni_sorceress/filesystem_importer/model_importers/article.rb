# frozen_string_literal: true

module OniSorceress::FilesystemImporter::ModelImporters::Article
  # https://github.com/rails/rails/blob/main/activestorage/app/models/active_storage/blob.rb#L349
  def self.compute_checksum_in_chunks(io)
    raise ArgumentError, "io must be rewindable" unless io.respond_to?(:rewind)

    OpenSSL::Digest.new("MD5").tap do |checksum|
      read_buffer = "".b
      checksum << read_buffer while io.read(5.megabytes, read_buffer)

      io.rewind
    end.base64digest
  end

  def self.call(root_path:)
    raw_models = ::OniSorceress::Filesystem::FileRepository.all(
      dir: root_path,
      file_formats: ["md", "text", "txt", "html", "htm"],
      load: true,
      model: "articles",
    )

    raw_models.each do |model|
      model_metadata, pure_body = ::OniSorceress::TextFileProcessor.split_metadata_and_content(text: model[:body])

      next if !model_metadata["posted"]
      next if !model_metadata["poster"]
      next if !model_metadata["license"]

      mime = model[:headers][:path].end_with?(".md") ? "text/markdown" : "text/plain"

      posted = model_metadata["posted"].max_by { |datechanges| datechanges.split(" ", 2)[0] }
      created = model_metadata["created"]&.max_by { |datechanges| datechanges.split(" ", 2)[0] }
      updated = model_metadata["updated"]&.max_by { |datechanges| datechanges.split(" ", 2)[0] }
      hidden = model_metadata["hidden"].present? && model_metadata["hidden"] != "false"

      world = ::OniSorceress::Content::World.find_by(name: "root")

      if !world
        world = ::OniSorceress::Content::World.create!(name: "root")
      end

      author = ::OniSorceress::Content::Author.find_by(name: model_metadata["poster"].first, world:)

      if !author
        author = ::OniSorceress::Content::Author.create!(name: model_metadata["poster"].first, world:)
      end

      license = ::OniSorceress::Content::License.find_by(abbr: model_metadata["license"].first, world:)

      if !license
        license = ::OniSorceress::Content::License.create!(abbr: model_metadata["license"].first, name: model_metadata["license"].first, world:)
      end

      license_abbr = license.abbr
      license_name = if license_abbr == "CC4-BY-SA"
        "Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)"
      elsif license_abbr == "CC4.0 BY"
        "Attribution 4.0 International (CC BY 4.0)"
      else
        license_abbr
      end

      license_link = if license_abbr == "CC4-BY-SA"
        "https://creativecommons.org/licenses/by-sa/4.0/"
      elsif license_abbr == "CC4.0 BY"
        "https://creativecommons.org/licenses/by/4.0/"
      end

      license.update!(name: license_name, link: license_link)

      first_level = ::OniSorceress::TextFileProcessor.first_level_category(url: model[:headers][:path], base_path: model[:headers][:content_dir])
      second_level = ::OniSorceress::TextFileProcessor.second_level_category(url: model[:headers][:path], base_path: model[:headers][:content_dir])
      category = ::OniSorceress::Content::Category.find_by(first_level:, second_level:, world:)

      if !category
        category = ::OniSorceress::Content::Category.create!(first_level:, second_level:, world:)
      end

      payload = {
        author:,
        body: pure_body,
        body_mime: mime,
        category:,
        created_at: created || updated || posted,
        hidden:,
        license:,
        original_filename: model[:headers][:path],
        posted_at: posted,
        pretty_id: model[:headers][:uri_id],
        secret: nil,
        title: ::OniSorceress::TextFileProcessor.title(text: pure_body),
        title_mime: mime,
        uri: model[:headers][:uri],
        last_updated_at: updated || posted,
        world:,
      }

      article = ::OniSorceress::Content::Article.find_by(pretty_id: model[:headers][:uri_id], world:)

      if article
        article.update!(payload)
      else
        ::OniSorceress::Content::Article.transaction do
          article = ::OniSorceress::Content::Article.create!(payload)
        end
      end

      model_metadata["language"].each_with_index.reduce([]) do |_result, (code, index)|
        language = ::OniSorceress::Content::Language.find_or_create_by!(code:, world:)
        articles_language = ::OniSorceress::Content::ArticlesLanguage.find_or_create_by!(article:, language:)
        articles_language.update!(priority: index)
      end

      # import images
      # TODO: check also checksum/other attribute
      imported_images = model[:image_filenames].map { |image_filename| File.basename(image_filename) }
      article.images.each do |image|
        if !image.file.filename.to_s.in?(imported_images)
          image.destroy
        end
      end

      model[:image_filenames].each do |image_filename|
        article.images.create!(file: File.open(image_filename))
      end

      # import videos
      imported_videos = model[:video_filenames].map { |video_filename| File.basename(video_filename) }
      article.videos.each do |video|
        if !video.file.filename.to_s.in?(imported_videos)
          video.destroy
        end
      end

      model[:video_filenames].each do |video_filename|
        article.videos.create!(file: File.open(video_filename))
      end

      # import galleries
      model[:image_galleries].each do |imported_image_gallery|
        name = imported_image_gallery[:name]
        imported_image_filenames = imported_image_gallery[:image_filenames]

        gallery = article.image_galleries.find_or_create_by!(name:)

        imported_images = imported_image_filenames.map do |image_filename|
          image_io = File.open(image_filename)
          {
            io: image_io,
            checksum: compute_checksum_in_chunks(image_io),
            filename: File.basename(image_filename),
          }
        end

        gallery.images.each do |image|
          # remove old duplicites
          duplicate_images = gallery.images.select { |img| img.file.filename == image.file.filename }
          if duplicate_images.size > 1
            duplicate_images.each(&:destroy)
          end

          # check checksum if image changed
          if !imported_images.find { |img| img[:checksum] == image.file.checksum && img[:filename] == image.file.filename }
            image.destroy
          end
        end

        imported_images.each do |img|
          gallery.images.create!(file: img[:io])
        end
      end
    end
  end
end
