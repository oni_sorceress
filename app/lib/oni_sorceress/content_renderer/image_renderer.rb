# frozen_string_literal: true

module ::OniSorceress::ContentRenderer::ImageRenderer
  def self.convert(images:, text:)
    img_regex = /{{image:.*}}/
    new_text = text.dup

    new_text.scan(img_regex).each do |match|
      image_attributes = Hash[match
        .gsub(/{|}/, "")
        .split(";;")
        .map { |s| s.split(":") }
      ]

      image = images.find { |img| img.file.filename == image_attributes["image"] }
      next if !image

      image_html_code = generate_image_html(image:, title: image_attributes["text"])
      new_text.sub!(match, image_html_code)
    end

    new_text
  end

  def self.generate_image_html(image:, title:)
    ::OniSorceress::Component.render(
      "image",
      component_variables: {
        versions: get_image_versions(image:),
        title:,
      },
    )
  end

  def self.get_image_versions(image:)
    image_variants = image.class.attachment_reflections["file"].named_variants

    image_versions = image_variants.reduce([]) do |result, (name, named_variant)|
      breakpoint =  named_variant.transformations[:resize_to_limit][0]
      mimetype = if named_variant.transformations[:format] == :jpeg
        "image/jpeg"
      elsif named_variant.transformations[:format] == :png
        "image/png"
      elsif named_variant.transformations[:format] == :heif && named_variant.transformations[:saver][:compression] == "av1"
        "image/avif"
      elsif named_variant.transformations[:format] == :heif && named_variant.transformations[:saver][:compression] == "hevc"
        "image/heif"
      end

      next result if !mimetype

      result << ::OniSorceress::Components::Image::Component::ImageVersion.new(
        mimetype: mimetype,
        breakpoint: breakpoint,
        url: Rails.application.routes.url_helpers.rails_blob_path(
          image.file.variant(name.to_sym),
          only_path: true,
        ),
      )

      result
    end

    image_versions
  end
end
