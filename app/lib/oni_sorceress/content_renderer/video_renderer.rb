# frozen_string_literal: true

module ::OniSorceress::ContentRenderer::VideoRenderer
  def self.convert(videos:, text:)
    video_regex = /{{video:.*}}/
    new_text = text.dup

    new_text.scan(video_regex).each do |match|
      video_attributes = Hash[match
        .gsub(/{|}/, "")
        .split(";;")
        .map { |s| s.split(":") }
      ]

      video = videos.find { |vid| vid.file.filename == video_attributes["video"] }
      next if !video

      video_html_code = generate_video_html(video:, title: video_attributes["title"])
      new_text.sub!(match, video_html_code)
    end

    new_text
  end

  def self.generate_video_html(video:, title:)
    file = video.file
    preview_url = Rails.application.routes.url_helpers.rails_blob_path(file.preview(resize_to_limit: [1080, 1080]), only_path: true)
    height = file.metadata[:height]
    width = file.metadata[:width]

    video_versions = [
      ::OniSorceress::Components::Video::Component::VideoVersion.new(
        mimetype: file.content_type,
        url: Rails.application.routes.url_helpers.rails_blob_path(file, only_path: true),
      ),
    ]

    ::OniSorceress::Component.render(
      "video",
      component_variables: {
        versions: video_versions,
        preview_url:,
        title:,
        height:,
        width:,
      },
    )
  end
end
