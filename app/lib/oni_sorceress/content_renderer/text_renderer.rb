# frozen_string_literal: true

require "kramdown"
require "RedCloth"

module ::OniSorceress::ContentRenderer::TextRenderer
  def self.convert(filename:, text:)
    extension = File.extname(filename)

    case extension
    when ".md", ".mkd", ".markdown"
      markdown(text:)
    when ".htm", ".html"
      html(text:)
    when ".txt", ".text"
      plain(text:)
    when ".textile"
      textile(text:)
    else
      raise "format not supported"
    end
  end

  def self.html(text:)
    text
  end

  def self.markdown(text:)
    Kramdown::Document.new(text).to_html
  end

  def self.plain(text:)
    text.gsub("&", "&amp;")
      .gsub("<", "&lt;")
      .gsub(">", "&gt;")
      .gsub("\n", "<br>")
  end

  def self.textile(text:)
    RedCloth.new(text).to_html
  end
end
