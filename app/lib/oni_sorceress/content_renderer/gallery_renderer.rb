# frozen_string_literal: true

module ::OniSorceress::ContentRenderer::GalleryRenderer
  def self.convert(galleries:, text:)
    img_regex = /{{gallery:.*}}/
    new_text = text.dup

    new_text.scan(img_regex).each do |match|
      gallery_attributes = Hash[match
        .gsub(/{|}/, "")
        .split(";;")
        .map { |s| s.split(":") }
      ]

      gallery = galleries.find { |glr| glr.name == gallery_attributes["gallery"] }
      next if !gallery

      gallery_html_code = generate_gallery_html(gallery:)
      new_text.sub!(match, gallery_html_code)
    end

    new_text
  end

  def self.generate_gallery_html(gallery:)
    # TODO: fix n+1
    rendered_images = gallery.images.order(created_at: :desc).map do |image|
      ::OniSorceress::ContentRenderer::ImageRenderer.generate_image_html(image:, title: nil)
    end

    ::OniSorceress::Component.render(
      "gallery",
      component_variables: {
        images_html: rendered_images,
        title: gallery.name,
      },
    )
  end
end
