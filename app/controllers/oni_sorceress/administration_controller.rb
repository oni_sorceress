# frozen_string_literal: true

class ::OniSorceress::AdministrationController < OniSorceress::ApplicationController
  http_basic_authenticate_with name: ENV.fetch("ADMINISTRATION_USER", nil), password: ENV.fetch("ADMINISTRATION_PASSWORD", nil)
end
