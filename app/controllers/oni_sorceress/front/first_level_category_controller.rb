# frozen_string_literal: true

class ::OniSorceress::Front::FirstLevelCategoryController < OniSorceress::ApplicationController
  def show
    @category = ::OniSorceress::Content::Category
      .in_world(@world)
      .find_by(first_level: params[:first_level])

    return handle_not_found if !@category

    @articles = ::OniSorceress::Content::Article
      .listable_for_first_level_category(world: @category.world.name, name: @category.first_level)
      .page(params[:page])

    handle_not_found if @articles.empty?
  end
end
