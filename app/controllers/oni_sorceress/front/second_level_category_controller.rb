# frozen_string_literal: true

class ::OniSorceress::Front::SecondLevelCategoryController < OniSorceress::ApplicationController
  def show
    @category = ::OniSorceress::Content::Category
      .in_world(@world)
      .find_by(first_level: params[:first_level], second_level: params[:second_level])

    return handle_not_found if !@category

    @articles = @category
      .articles
      .listable(world: @world)
      .page(params[:page])

    handle_not_found if @articles.empty?
  end
end
