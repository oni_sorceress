# frozen_string_literal: true

class ::OniSorceress::Front::AuthorsController < OniSorceress::ApplicationController
  def show
    @author = ::OniSorceress::Content::Author
      .for_world(@world)
      .find_by(name: params[:id])

    return handle_not_found if !@author

    handle_not_found if @author.articles.count > 0 && @author.articles.where(hidden: true).count == @author.articles.count
  end
end
