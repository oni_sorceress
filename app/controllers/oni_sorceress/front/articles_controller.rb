# frozen_string_literal: true

class ::OniSorceress::Front::ArticlesController < OniSorceress::ApplicationController
  def show
    @article = ::OniSorceress::Content::Article
      .includes(:author, :category, :world)
      .find_by(pretty_id: params[:id], world: { name: @world })

    return handle_not_found if !@article || (@article.hidden && bot?(request))

    if @article.hidden
      @private_page = true
    end

    @meta_language = @article
      .articles_languages
      .includes(:language)
      .order(:priority)
      .map { |article_language| article_language.language.code }

    @comment = @article.comments.build
    @comments = @article.comments.where.not(published_at: nil).order(created_at: :desc)
  end

  private

  def bot?(request)
    user_agent = request.headers["HTTP_USER_AGENT"].to_s.downcase
    ["bot", "crawl", "checker", "archiver", "transcoder", "spider", "preview", "agent", "generator", "scraper", "wiki", "reader"].any? do |word|
      user_agent.include?(word)
    end
  end
end
