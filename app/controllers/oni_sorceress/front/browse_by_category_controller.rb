# frozen_string_literal: true

class ::OniSorceress::Front::BrowseByCategoryController < OniSorceress::ApplicationController
  def index
    articles = ::OniSorceress::Content::Article
      .listable(world: @world)

    return handle_not_found if articles.empty?

    @articles_by_category = articles
      .group_by { |article| "#{article.category.first_level}/#{article.category.second_level}" }
  end
end
