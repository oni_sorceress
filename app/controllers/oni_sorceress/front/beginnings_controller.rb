# frozen_string_literal: true

class ::OniSorceress::Front::BeginningsController < OniSorceress::ApplicationController
  def index
    articles = ::OniSorceress::Content::Article.listable(world: @world)

    @total_count = articles.count
    @latest_articles = articles.page(params[:page]).per(10)

    handle_not_found if articles.empty?
  end
end
