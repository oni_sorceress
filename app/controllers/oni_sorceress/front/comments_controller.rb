# frozen_string_literal: true

class ::OniSorceress::Front::CommentsController < OniSorceress::ApplicationController
  invisible_captcha only: [:create], honeypot: :poster

  def create
    article = ::OniSorceress::Content::Article.find(params[:article_id])

    if ::OniSorceress::Content::Comment.count >= 100_000
      redirect_to(article_path(article.pretty_id), alert: t("oni_sorceress.controllers.front.comments.too_many")) and return
    end

    comment = article.comments.create(comment_params)
    remote_request = comment.build_request

    cookies_headers = ["HTTP_COOKIE"]
    # 1. predefined
    # 2. $scheme - rack is for HTTP, won't handle other protocols here
    # 3. http headers
    # 4. REQUEST_URI seems to be defined like everywhere (PHP, Python, Rack), but is not a part of CGI spec
    #    ORIGINAL_FULLPATH and ORIGINAL_SCRIPT_NAME is Rack specific
    base_headers = [
      "AUTH_TYPE",
      "CONTENT_LENGTH",
      "CONTENT_TYPE",
      "GATEWAY_INTERFACE",
      "PATH_INFO",
      "PATH_TRANSLATED",
      "QUERY_STRING",
      "REMOTE_ADDR",
      "REMOTE_HOST",
      "REMOTE_IDENT",
      "REMOTE_USER",
      "REQUEST_METHOD",
      "SCRIPT_NAME",
      "SERVER_NAME",
      "SERVER_PORT",
      "SERVER_PROTOCOL",
      "SERVER_SOFTWARE",
    ]
    protocol_headers = ["HTTP", "HTTPS"]
    cgi_headers = ["REQUEST_URI", "ORIGINAL_FULLPATH", "ORIGINAL_SCRIPT_NAME"]

    remote_request.data = request.env.select do |header, value|
      # don't bother with values other than a string
      next if !value.is_a?(String)
      # https://tools.ietf.org/html/rfc3875 CGI 1.1 spec
      # reject cookies
      next if cookies_headers.include?(header)

      base_headers.include?(header) || protocol_headers.include?(header) || header =~ /\AHTTP_\w+\z/ || cgi_headers.include?(header)
    end
    remote_request.data[:uid] = request.env["action_dispatch.request_id"] || SecureRandom.uuid

    if !comment.valid? || !remote_request.valid? || !comment.save || !remote_request.save
      redirect_to(article_path(article.pretty_id), alert: t("oni_sorceress.controllers.front.comments.invalid"))
    else
      bogofilter = Bogofilter.new(dbpath: Rails.application.config.bogofilter_database)
      bogofilter_result = bogofilter.classify(comment.poster_name.to_s + comment.body.to_s)
      spam_check = ::OniSorceress::Analytics::SpamCheck.new(classifier: :bogofilter, **bogofilter_result)
      comment.spam_checks << spam_check

      redirect_to(article_path(article.pretty_id), notice: t("oni_sorceress.controllers.front.comments.created"))
    end
  end

  private

  def comment_params
    params.expect(oni_sorceress_content_comment: [:poster_name, :body])
  end
end
