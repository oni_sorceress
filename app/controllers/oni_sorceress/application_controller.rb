# frozen_string_literal: true

class ::OniSorceress::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

  before_action :set_page_metadata
  layout "oni_sorceress/layouts/application"

  def set_page_metadata
    world = request.subdomains.join(".")
    @world = world.blank? || world == "www" ? "root" : world

    @default_language = ENV.fetch("META_DEFAULT_LANGUAGE") { "en" }
    @title_suffix = ENV.fetch("META_TITLE_SUFFIX") { ";; Oni Sorceress ;;" }
    @meta_description = ENV.fetch("META_DESCRIPTION") { nil }

    portrait_dir = Rails.public_path.join("media/backgrounds/portrait")
    landscape_dir = Rails.public_path.join("media/backgrounds/landscape")

    time = Time.zone.now
    bg_portraits = Dir["#{portrait_dir}/*"]
      .map { |image| File.basename(image).rpartition("_")[0] }
      .uniq
    bg_landscapes = Dir["#{landscape_dir}/*"]
      .map { |image| File.basename(image).rpartition("_")[0] }
      .uniq

    if !bg_portraits.empty?
      bg_portrait = bg_portraits[time.yday % bg_portraits.size]
      @bg_portrait = "/media/backgrounds/portrait/#{bg_portrait}"
    end

    if !bg_landscapes.empty?
      bg_landscape = bg_landscapes[time.yday % bg_landscapes.size]
      @bg_landscape = "/media/backgrounds/landscape/#{bg_landscape}"
    end
  end

  private

  def handle_not_found
    render(
      plain: "(page not found) The page you were looking for doesn't exist. You may have mistyped the address or the page may have moved.",
      status: :not_found,
    ) and return
  end
end
