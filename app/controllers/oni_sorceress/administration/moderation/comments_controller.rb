# frozen_string_literal: true

class ::OniSorceress::Administration::Moderation::CommentsController < OniSorceress::AdministrationController
  def index
    @comments = ::OniSorceress::Content::Comment.order(created_at: :desc)
    # @comments = @comments.where(params[:filters])
    # @comments = @comments.order(params[:sort][:name] => params[:sort][:direction])
    @comments = @comments.page(params[:page]).per(params[:per])
  end
end
