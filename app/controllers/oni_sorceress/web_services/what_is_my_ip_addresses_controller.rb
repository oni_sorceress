# frozen_string_literal: true

class OniSorceress::WebServices::WhatIsMyIpAddressesController < OniSorceress::ApplicationController
  def show
    @ip_address = request.headers["REMOTE_ADDR"]
  end
end
