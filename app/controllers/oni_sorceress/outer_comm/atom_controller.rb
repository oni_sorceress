# frozen_string_literal: true

class ::OniSorceress::OuterComm::AtomController < OniSorceress::ApplicationController
  def index
    @articles = ::OniSorceress::Content::Article
      .listable(world: @world)
      .where(last_updated_at: (3.months.ago..))

    newest_article = @articles.first
    @last_updated = newest_article ? newest_article.last_updated_at : Time.zone.local(1970, 1, 1)

    respond_to do |format|
      format.atom { render }
    end
  end
end
