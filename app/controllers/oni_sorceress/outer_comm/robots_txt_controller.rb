# frozen_string_literal: true

class ::OniSorceress::OuterComm::RobotsTxtController < OniSorceress::ApplicationController
  layout false

  def show
  end
end
