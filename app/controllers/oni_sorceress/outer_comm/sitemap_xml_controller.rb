# frozen_string_literal: true

class ::OniSorceress::OuterComm::SitemapXmlController < OniSorceress::ApplicationController
  layout false

  def show
    @first_level_categories = ::OniSorceress::Content::Category.in_world_first_level(@world)
    @categories = ::OniSorceress::Content::Category.in_world(@world)
    @articles = ::OniSorceress::Content::Article.listable(world: @world)
  end
end
