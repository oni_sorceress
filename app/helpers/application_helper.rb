# frozen_string_literal: true

module ApplicationHelper
  def _(text)
    text
  end

  def render_article(article)
    rendered = ::OniSorceress::ContentRenderer::TextRenderer.convert(
      filename: article.original_filename,
      text: article.body,
    )
    rendered = ::OniSorceress::ContentRenderer::ImageRenderer.convert(
      images: article.images,
      text: rendered,
    )
    rendered = ::OniSorceress::ContentRenderer::GalleryRenderer.convert(
      galleries: article.image_galleries,
      text: rendered,
    )
    ::OniSorceress::ContentRenderer::VideoRenderer.convert(
      videos: article.videos,
      text: rendered,
    )
  end
end
